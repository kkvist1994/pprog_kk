void print_vector(const gsl_vector*, int);
void print_matrix(const gsl_matrix*, int);
void rkstep23(
	double,                                          /* the current value of the variable */
	double,                                          /* the step to be taken */
  gsl_vector*,
  gsl_vector*,
  gsl_vector*,
  gsl_vector*,
  gsl_vector*,
	gsl_vector*,                                    /* the current value y(t) of the sought function */
	void f(double, gsl_vector*, gsl_vector*), /* the right-hand-side, dydt = f(t,y) */
	gsl_vector*,                                   /* output: y(t+h) */
	gsl_vector*);

  int driver(
  	double*,                             /* the current value of the variable */
  	double,                              /* the end-point of the integration */
  	double*,                             /* the current step-size */
  	gsl_vector*,                        /* the current y(t) */
  	double,                            /* absolute accuracy goal */
  	double,                            /* relative accuracy goal */
    gsl_vector*,                     /*Vector to store the ypath*/
    gsl_matrix*,                     /*Matrix to store the ypath*/
    gsl_vector*,
    gsl_vector*,
    gsl_vector*,
    gsl_vector*,
    gsl_vector*,
  	void stepper(                          /* the stepper function to be used */
      double,                                          /* the current value of the variable */
    	double,                                          /* the step to be taken */
      gsl_vector*,
      gsl_vector*,
      gsl_vector*,
      gsl_vector*,
      gsl_vector*,
    	gsl_vector*,                                    /* the current value y(t) of the sought function */
    	void f(double, gsl_vector*, gsl_vector*), /* the right-hand-side, dydt = f(t,y) */
    	gsl_vector*,                                   /* output: y(t+h) */
    	gsl_vector*
      ),
  	void f(double, gsl_vector*, gsl_vector*) /* right-hand-side */
  );

  double dot(gsl_vector*, gsl_vector*);

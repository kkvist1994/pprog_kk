#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"ode.h"
#include<gsl/gsl_blas.h>

void print_vector(const gsl_vector* v, int k){
  printf("[");
  for(int i = 0; i < k; i++){
    if (i != k-1) printf("%.20f\n", gsl_vector_get(v, i));
    else printf("%.20f]\n\n", gsl_vector_get(v, i));
  }
}

void print_matrix(const gsl_matrix* m, int k){
  printf("[");
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < k; j++){
      if (j == k-1){
        if (i == m->size1-1) printf("%.3f]\n\n", gsl_matrix_get(m, i, j));
        else printf("%.3f\n", gsl_matrix_get(m, i, j));
      }
      else printf("%.3f ", gsl_matrix_get(m, i, j));
    }
  }
}

double dot(gsl_vector* a, gsl_vector* b){
  assert(a->size == b->size);
  double res = 0;
  for(int i = 0; i < a->size; i++){
    res += gsl_vector_get(a, i)*gsl_vector_get(b, i);
  }
  return res;
}

void rkstep23(
	double t,                                          /* the current value of the variable */
	double h,                                          /* the step to be taken */
  gsl_vector* k1,
  gsl_vector* k2,
  gsl_vector* k3,
  gsl_vector* k4,
  gsl_vector* k_calc,
	gsl_vector* yt,                                    /* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt), /* the right-hand-side, dydt = f(t,y) */
	gsl_vector* yth,                                   /* output: y(t+h) */
	gsl_vector* err){                                  /* output: error estimate dy */
    gsl_vector_memcpy(k_calc, yt);
    gsl_blas_daxpy(0.5*h, k1, k_calc);
    f(t + 0.5*h, k_calc, k2);
    gsl_vector_memcpy(k_calc, yt);
    gsl_blas_daxpy(0.75*h, k2, k_calc);
    f(t + 0.75*h, k_calc, k3);
    /*Now we can utilize that the a coefficients for k4 are the same as the b coefficients*/
    gsl_vector_memcpy(yth, yt);
    gsl_blas_daxpy(2.0/9*h, k1, yth);
    gsl_blas_daxpy(1.0/3*h, k2, yth);
    gsl_blas_daxpy(4.0/9*h, k3, yth);
    f(t + h, yth, k4);
    gsl_vector_memcpy(k_calc, yt);
    gsl_blas_daxpy(7.0/24*h, k1, k_calc);
    gsl_blas_daxpy(1.0/4*h, k2, k_calc);
    gsl_blas_daxpy(1.0/3*h, k3, k_calc);
    gsl_blas_daxpy(1.0/8*h, k4, k_calc);
    gsl_vector_memcpy(err, yth);
    gsl_blas_daxpy(-1.0, yt, err);
    gsl_vector_memcpy(yt, yth);
    gsl_vector_memcpy(k1, k4);
}

int driver(
	double* t,                             /* the current value of the variable */
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	gsl_vector* yt,                        /* the current y(t) */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
  gsl_vector* xpath,                     /*Vector to store the ypath*/
  gsl_matrix* ypath,                     /*Matrix to store the ypath*/
  gsl_vector* k1,
  gsl_vector* k2,
  gsl_vector* k3,
  gsl_vector* k4,
  gsl_vector* k_calc,
	void stepper(                          /* the stepper function to be used */
    double t,                                          /* the current value of the variable */
  	double h,                                          /* the step to be taken */
    gsl_vector* k1,
    gsl_vector* k2,
    gsl_vector* k3,
    gsl_vector* k4,
    gsl_vector* k_calc,
  	gsl_vector* yt,                                    /* the current value y(t) of the sought function */
  	void f(double t, gsl_vector* y, gsl_vector* dydt), /* the right-hand-side, dydt = f(t,y) */
  	gsl_vector* yth,                                   /* output: y(t+h) */
  	gsl_vector* err
    ),
	void f(double t, gsl_vector* y, gsl_vector* dydt) /* right-hand-side */
){
  f(*t, yt, k1);
  double err, normy, tol, a = gsl_vector_get(xpath, 0);
  gsl_vector* sol_handle = gsl_vector_alloc(yt->size);
  gsl_vector* err_handle = gsl_vector_alloc(yt->size);
  int k = 0;
  while(*t < b){
    *t = gsl_vector_get(xpath, k);
    gsl_matrix_get_col(yt, ypath, k);
    if(*t+*h > b){*h = b - *t;}
    stepper(*t, *h, k1, k2, k3, k4, k_calc, yt, f, sol_handle, err_handle);
    err = sqrt(dot(err_handle, err_handle));
    normy = sqrt(dot(sol_handle, sol_handle));
    tol = (normy*eps + acc)*sqrt(*h/(b-a));
    if(err<tol){
      k++;
      if(k>xpath->size-1){printf("Maximum number of steps exceeded\n"); return k;}
      gsl_vector_set(xpath, k, *t+*h);
      gsl_matrix_set_col(ypath, k, sol_handle);
      }
      if(err>0) *h*=pow(tol/err, 0.25)*0.95; else *h*=2;
    }
    gsl_vector_free(sol_handle);
    gsl_vector_free(err_handle);
    return k+1;
  }

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"ode.h"
#include<gsl/gsl_blas.h>

void orbit(double t, gsl_vector* y, gsl_vector* dydt){
  gsl_vector_set(dydt, 0, gsl_vector_get(y, 1));
  gsl_vector_set(dydt, 1, 1 + gsl_vector_get(y, 0)*(0.01*gsl_vector_get(y, 0)-1));
}

int main(){
  double t = 0; double h = 0.01;
  double *t_p = &t, *h_p = &h;
  double b = 200*M_PI;
  int n = 2, max = 1000000;
  gsl_vector* ut = gsl_vector_alloc(n);
  gsl_vector_set(ut, 0, 1);
  gsl_vector_set(ut, 1, -0.5);
  gsl_vector* k1 = gsl_vector_alloc(n);
  gsl_vector* k2 = gsl_vector_alloc(n);
  gsl_vector* k3 = gsl_vector_alloc(n);
  gsl_vector* k4 = gsl_vector_alloc(n);
  gsl_vector* k_calc = gsl_vector_alloc(n);
  gsl_vector* xpath = gsl_vector_alloc(max);
  gsl_vector_set(xpath, 0, *t_p);
  gsl_matrix* ypath = gsl_matrix_alloc(n, max);
  gsl_matrix_set_col(ypath, 0, ut);
  printf("I solved the system of differential equations modeling the rekativistic\n");
  printf("precession of a planet, using the Bogacki and Shampine method,\n");
  printf("while utilizing the first same as last property of this methid.\n");
  printf("The traced path is shown in the file plot.svg.\n");
  int k = driver(t_p, b, h_p, ut, 1, 1, xpath, ypath, k1, k2, k3, k4, k_calc, rkstep23, orbit);
  printf("The number of steps used to solve 100 revolutions of the orbit was %d.\n", k);
  FILE* datastream = fopen("data.txt", "w");
  for(int i = 0; i<k; i++){
    fprintf(datastream, "%.5f\t%.5f\n", gsl_vector_get(xpath, i), gsl_matrix_get(ypath, 0, i));
  }
  fclose(datastream);
  return 0;
}

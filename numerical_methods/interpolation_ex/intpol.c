#include<assert.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include"intpol.h"

void print_array(int n, double* a){
	printf("[");
	for(int i = 0; i < n; i++){
		if(i != n-1){printf("%.3f, ", a[i]);}
		else{printf("%.3f]\n", a[i]);}
	}
}

void print_spline(qspline* s){
	print_array(s->n, s->x);
	print_array(s->n, s->y);
	print_array(s->n-1, s->b);
	print_array(s->n-1, s->c);
}

xy_data* initialize_xy(double a, double b, double Dx, double (*f)(double)){
	xy_data* xy = (xy_data*) malloc(sizeof(xy_data));
	int n_data = (b-a)/Dx + 1;
	xy->n = n_data;
  double x = a;
  xy->x_data = (double *) malloc(n_data*sizeof(double));
  xy->y_data = (double *) malloc(n_data*sizeof(double));
  for(int i = 0; i <= n_data; i++){
    xy->x_data[i] = x;
    xy->y_data[i] = f(x);
    x += Dx;
  }
	return xy;
}

int bin_search(int n, double* x, double z){
	int i = 0, j=n-1;
	 while (j - i > 1){
		 int m = (j+i)/2;
		 if (z > x[m]){i = m;}
		 else {j = m;}
	 }
	 return i;
}

double linterp (int n, double* x, double* y, double z){
	assert(n > 1 && z>=x[0] && z<=x[n-1]);
	/*Determine the index of the largest number in x that is smaller than z*/
	int i = bin_search(n, x, z);
	/*Form the straight line through (x[i], y[i]) and (x[i+1], y[i+1]) and
	evaluate in z*/
	return y[i] + (y[i+1] - y[i])/(x[i+1] - x[i])*(z - x[i]);
 }

double linterp_integ(int n, double* x, double* y, double z){
	assert(n > 1 && z>=x[0] && z<=x[n-1]);
	int i = bin_search(n, x, z);
	/*Iterate over intervals preceeding the one including z*/
	int k = 1;
	double a;
	double integral = 0;
	while(k <= i){
		a = (y[k] - y[k-1])/(x[k] - x[k-1]);
		integral += 0.5*a*pow((x[k] - x[k-1]), 2) + y[k-1]*(x[k] - x[k-1]);
		k++;
	}
	a = (y[i+1] - y[i])/(x[i+1] - x[i]);
	integral += 0.5*a*pow((z - x[i]), 2) + y[i]*(z - 	x[i]);
	return integral;
}

double squared_expo(double x){
	return exp(2*x);
}

double squared_expo_integ(double x){
	return 0.5*exp(2*x);
}

qspline* qspline_alloc(int n , double* x, double* y){
  qspline* s = (qspline*) malloc(sizeof(qspline));
	s->x = (double*) malloc(n*sizeof(double));
  s->y = (double*) malloc(n*sizeof(double));
	s->b = (double*) malloc((n-1)*sizeof(double));
  s->c = (double*) malloc((n-1)*sizeof(double));
  s->n = n;
  for(int i = 0; i < n; i++){
    s->x[i] = x[i]; s->y[i] = y[i];
  }
  int i; double p[n-1], h[n-1];
  for(i = 0; i < n-1; i++){
    h[i] = x[i+1] - x[i];
    p[i] = (y[i+1]-y[i])/h[i];
  }
  s->c[0] = 0;
  for(i = 0; i < n-2; i++){
    s->c[i+1] = (p[i+1] - p[i] - s->c[i]*h[i])/h[i+1];
  }
  s->c[n-2]/=2;
  for(i = n-3; i >= 0; i--){
    s->c[i] = (p[i+1] - p[i] - s->c[i+1]*h[i+1])/h[i];
  }
	for(i = 0; i < n-1; i++){
		s->b[i] = p[i] - s->c[i]*h[i];
	}
  return s;
}

double qspline_eval(qspline *s, double z){
  assert(z >= s->x[0] && z <= s->x[s->n - 1]);
  int i = bin_search(s->n, s->x, z);
	double h = z - s->x[i];
	return s->y[i] + h*(s->b[i] + h*s->c[i]);
}

double qspline_derivative(qspline *s, double z){
	assert(z >= s->x[0] && z <= s->x[s->n - 1]);
	int i = bin_search(s->n, s->x, z);
	return s->b[i] + 2*s->c[i]*(z - s->x[i]);
}

double qspline_integral(qspline *s, double z){
	assert(z >= s->x[0] && z <= s->x[s->n - 1]);
	int i = bin_search(s->n, s->x, z);
	double integral = 0;
	int k = 0;
	while(k < i){
		integral += qint_aux(s->x[k], s->c[k], s->b[k], s->y[k], s->x[k+1]);
		k++;
	}
	integral += qint_aux(s->x[k], s->c[k], s->b[k], s->y[k], z);
	return integral;
}

double qint_aux(double x_i, double c_i, double b_i, double y_i, double z){
	return pow(z, 3)*c_i/3 + pow(z, 2)*(b_i/2 - c_i*x_i) + z*(c_i*pow(x_i, 2) - b_i*x_i + y_i)
		- (pow(x_i, 3)*c_i/3 - pow(x_i, 2)*b_i/2 + x_i*y_i);
}

void qspline_free(qspline* s){
	free(s->x); free(s->y); free(s->b); free(s->c); free(s);
}

void xy_free(xy_data* xy){
	free(xy->x_data); free(xy->y_data); free(xy);
}

cubic_spline* cubic_spline_alloc(int n, double* x, double* y){
	cubic_spline* s = (cubic_spline*) malloc(sizeof(cubic_spline));
	s->x = (double*) malloc(n*sizeof(double));
	s->y = (double*) malloc(n*sizeof(double));
	s->b = (double*) malloc(n*sizeof(double));
	s->c = (double*) malloc((n-1)*sizeof(double));
	s->d = (double*) malloc((n-1)*sizeof(double));
	s->n = n;
	for(int i = 0; i<n; i++){
		s->x[i] = x[i]; s->y[i] = y[i];
	}
	double h[n-1], p[n-1];
	for(int i = 0; i < n-1; i++){
		h[i] = x[i+1]-x[i]; assert(h[i]>0); /*Checks that the list is ordered*/
		p[i] = (y[i+1] - y[i])/h[i];
	}
	double D[n], Q[n-1], B[n];
	D[0] = 2;
	for(int i = 0; i < n-2; i++){
		D[i+1] = 2*h[i]/h[i+1]+2;
	}
	D[n-1] = 2;
	Q[0] = 1;
	for(int i = 0; i < n-2; i++){
		Q[i+1] = h[i]/h[i+1];
	}
	for(int i = 0; i < n-2; i++){
		B[i+1] = 3*(p[i] + p[i+1]*h[i]/h[i+1]);
		}
		B[0] = 3*p[0]; B[n-1] = 3*p[n-2];
		for(int i = 1; i < n; i++){
			D[i] -= Q[i-1]/D[i-1];
			B[i] -= B[i-1]/D[i-1];
		}
		s->b[n-1] = B[n-1]/D[n-1];
		for(int i = n-2; i>=0; i--){
			s->b[i] = (B[i] - Q[i]*s->b[i+1])/D[i];
		}
		for(int i = 0; i < n-1; i++){
			s->c[i] = (-2*s->b[i]-s->b[i+1]+3*p[i]/h[i]);
			s->d[i] = (s->b[i]+s->b[i+1]-2*p[i])/h[i]/h[i];
		}
		return s;
}

double cubic_spline_eval(cubic_spline* s, double z){
	assert(z >= s->x[0] && z <= s->x[s->n - 1]);
	int i = bin_search(s->n, s->x, z);
	double h = z-s->x[i];
	return s->y[i] + h*(s->b[i] + h*(s->c[i] + h*s->d[i]));
}

double cubic_spline_deriv(cubic_spline* s, double z){
	int i = bin_search(s->n, s->x, z);
	return s->b[i] + 2*s->c[i]*(z - s->x[i]) + 3*s->d[i]*pow(z-s->x[i], 2);
}

double cubic_spline_integ(cubic_spline* s, double z){
	int i = bin_search(s->n, s->x, z);
	double t_k, integral = 0;
	int k = 0;
	while(k < i){
		t_k = s->x[k+1] - s->x[k];
		integral += s->y[k]*t_k + s->b[k]*pow(t_k, 2)/2 + s->c[k]*
			pow(t_k, 3)/3 + s->d[k]*pow(t_k, 4)/4;
		k++;
	}
	t_k = z - s->x[k];
	integral += s->y[k]*t_k + s->b[k]*pow(t_k, 2)/2 + s->c[k]*
		pow(t_k, 3)/3 + s->d[k]*pow(t_k, 4)/4;
	return integral;
}

void cubic_spline_free(cubic_spline* s){
	free(s->x); free(s->y); free(s->b); free(s->c); free(s);
}

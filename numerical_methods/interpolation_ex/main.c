#include<math.h>
#include<stdio.h>
#include"intpol.h"
#include<stdlib.h>

void linear_plot(xy_data* xy, double a, double b, double dx){
  FILE* lin_stream = fopen("lin_data.txt", "w");
  double y_intpol, y_integrate, x_plot = a;
  int n_data = xy->n;
  while(x_plot < b){
    y_intpol = linterp(n_data, xy->x_data, xy->y_data, x_plot);
    y_integrate = linterp_integ(n_data, xy->x_data, xy->y_data, x_plot);
    fprintf(lin_stream, "%.5f %.5f %.5f %.5f %.5f\n",
    x_plot, squared_expo(x_plot), y_intpol,
    squared_expo_integ(x_plot), y_integrate);
    x_plot += dx;
  }
  fclose(lin_stream);
}

void quadratic_plot(xy_data* xy, double a, double b, double dx){
  FILE* quad_stream = fopen("quad_data.txt", "w");
  qspline* qs = qspline_alloc(xy->n , xy->x_data, xy->y_data);
  double y_intpol, y_deriv, y_integrate, x_plot = a;
  while(x_plot < b){
    y_intpol = qspline_eval(qs, x_plot);
    y_deriv = qspline_derivative(qs, x_plot);
    y_integrate = qspline_integral(qs, x_plot);
    fprintf(quad_stream, "%.5f %.5f %.5f %.5f %.5f %.5f %.5f\n",
    x_plot, squared_expo(x_plot), y_intpol,
    2*squared_expo(x_plot), y_deriv,
    squared_expo_integ(x_plot), y_integrate);
    x_plot += dx;
  }
  qspline_free(qs);
  fclose(quad_stream);
}

void cubic_plot(xy_data* xy, double a, double b, double dx){
  FILE* cubic_stream = fopen("cubic_data.txt", "w");
  cubic_spline* cs = cubic_spline_alloc(xy->n , xy->x_data, xy->y_data);
  double y_intpol, y_deriv, y_integrate, x_plot = a;
  while(x_plot < b){
    y_intpol = cubic_spline_eval(cs, x_plot);
    y_deriv = cubic_spline_deriv(cs, x_plot);
    y_integrate = cubic_spline_integ(cs, x_plot);
    fprintf(cubic_stream, "%.5f %.5f %.5f %.5f %.5f %.5f %.5f\n",
    x_plot, squared_expo(x_plot), y_intpol,
    2*squared_expo(x_plot), y_deriv,
    squared_expo_integ(x_plot), y_integrate);
    x_plot += dx;
  }
  cubic_spline_free(cs);
  fclose(cubic_stream);
}

int main(void){
  /*Testing linear interpolation*/
  double a = 0;
  double b = 5;
  double Dx = 1;
  xy_data* xy = initialize_xy(a, b, Dx, squared_expo);
  double dx = 0.1;
  linear_plot(xy, a, b, dx);
  quadratic_plot(xy, a, b, dx);
  cubic_plot(xy, a, b, dx);
  xy_free(xy);
  /*Testing quadratic interpolation*/
  /*double x[5] = {1, 2, 3, 4, 5};
  double y[5] = {1 ,2, 3, 4, 5};
  int n = 5;
  qspline* s = qspline_alloc(n, x, y);
  print_spline(s);
  qspline_free(s);*/
  return 0;
}

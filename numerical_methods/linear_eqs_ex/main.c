#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"lin_eq.h"
#include<gsl/gsl_blas.h>
#include<time.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_permutation.h>

int main(void){
  unsigned int seed = time(NULL);
  int n = 3;
  int m = 3;
  gsl_vector* b = gsl_vector_alloc(n);
  randomize_vec(b, seed);
  printf("Solving the linear set of equations Ax = b defined by:\n");
  printf("b = \n");
  print_vector(b);
  gsl_vector* x = gsl_vector_alloc(m);
  gsl_vector* test_vec = gsl_vector_alloc(n);
  gsl_matrix* test_mat = gsl_matrix_alloc(n, m);
  gsl_matrix* R = gsl_matrix_calloc(m, m);
  gsl_matrix* A = gsl_matrix_alloc(n, m);
  randomize_mat(A, seed);
  printf("A = \n");
  print_matrix(A);
  gsl_matrix_memcpy(test_mat, A);
  qr_gs_decomp(A, R);
  qr_gs_solve(A, R, b, x);
  printf("The solution is determined to: x =\n");
  print_vector(x);
  gsl_blas_dgemv(CblasNoTrans, 1.0, test_mat, x, 0.0, test_vec);
  printf("Testing the result by calculating Ax yields:\n");
  print_vector(test_vec);
  gsl_matrix* A_inverse = gsl_matrix_alloc(n ,n);
  gsl_matrix* test_inverse = gsl_matrix_alloc(n ,n);
  qr_gs_inverse(A, R, A_inverse);
  printf("The inverse of A is determined to:\n");
  print_matrix(A_inverse);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A_inverse, test_mat, 0.0, test_inverse);
  printf("The product of A_inv and A yields:\n");
  print_matrix(test_inverse);
  gsl_matrix_free(R);
  gsl_matrix_free(A);
  gsl_vector_free(x);
  gsl_vector_free(b);
  gsl_vector_free(test_vec);
  gsl_matrix_free(test_mat);
  gsl_matrix_free(A_inverse);
  gsl_matrix_free(test_inverse);

  printf("Golub-Kahan-Lanczos bidiagonalization:\n");
  gsl_matrix* A_g = gsl_matrix_alloc(n, n);
  randomize_mat(A_g, seed);
  printf("Perfoming bidiagonalization on A_g =\n");
  print_matrix(A_g);
  gsl_matrix* U_g = gsl_matrix_alloc(n, n);
  gsl_matrix* V_g = gsl_matrix_alloc(n, n);
  gsl_matrix* B_g = gsl_matrix_calloc(n, n);
  gsl_matrix* UA = gsl_matrix_alloc(n, n);
  gsl_matrix* UAV = gsl_matrix_alloc(n, n);
  GKLBP(A_g, U_g, V_g, B_g);
  printf("Yields the foolowing matrices:\n");
  gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, U_g, A_g, 0.0, UA);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, UA, V_g, 0.0, UAV);
  printf("B=\n");
  print_matrix(B_g);
  printf("U=\n");
  print_matrix(U_g);
  printf("V=\n");
  print_matrix(V_g);
  printf("Calculating U*AV to test that it is equal to B yields:\n");
  print_matrix(UAV);
  gsl_vector* b_g = gsl_vector_alloc(n);
  randomize_vec(b_g, seed);
  printf("Solving the linear set of equations A_g*x_g = b_g using bidiagonalization, where:\n");
  printf("A_g=\n");
  print_matrix(A_g);
  printf("b_g=\n");
  print_vector(b_g);
  gsl_vector* x_g = gsl_vector_alloc(n);
  gsl_vector* result_g = gsl_vector_alloc(n);
  GKLBP_solve(B_g, U_g, V_g, b_g, x_g);
  printf("Gives the solution x_g=\n");
  print_vector(x_g);
  gsl_blas_dgemv(CblasNoTrans, 1.0, A_g, x_g, 0.0, result_g);
  printf("A_g*x_g yields:\n");
  print_vector(result_g);
  gsl_matrix* test_inverse_g = gsl_matrix_alloc(n, n);
  gsl_matrix* inverse_res_g = gsl_matrix_alloc(n, n);
  GKLBP_inverse(B_g, U_g, V_g, test_inverse_g);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, test_inverse_g, A_g, 0.0, inverse_res_g);
  printf("The inverse A_g_inv was found using bidiagonalization. A_g_inv = \n");
  print_matrix(test_inverse_g);
  printf("A_g_inv*A yields:\n");
  print_matrix(inverse_res_g);
  printf("The absolute value of the determinant of A is calculated to be: %.5f\n", determinant(B_g));
  gsl_permutation* p = gsl_permutation_alloc(n);
  int* sig = &n;
  gsl_linalg_LU_decomp(A_g, p, sig);
  double determinant = gsl_linalg_LU_det(A_g, *sig);
  printf("For comparison a gsl-implementation calculates the determinant of A_g to: %.5f\n", determinant);
  gsl_matrix_free(A_g);
  gsl_matrix_free(U_g);
  gsl_matrix_free(V_g);
  gsl_matrix_free(B_g);
  gsl_matrix_free(UA);
  gsl_matrix_free(UAV);
  gsl_vector_free(b_g);
  gsl_vector_free(x_g);
  gsl_vector_free(result_g);
  gsl_matrix_free(test_inverse_g);
  gsl_matrix_free(inverse_res_g);
  gsl_permutation_free(p);
  return 0;
}

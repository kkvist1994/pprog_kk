#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"lin_eq.h"
#include<gsl/gsl_blas.h>

void print_vector(const gsl_vector* v){
  printf("[");
  for(int i = 0; i < v->size; i++){
    if (i != v->size-1) printf("%.3f\n", gsl_vector_get(v, i));
    else printf("%.3f]\n\n", gsl_vector_get(v, i));
  }
}

void print_matrix(const gsl_matrix* m){
  printf("[");
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < m->size2; j++){
      if (j == m->size2-1){
        if (i == m->size1-1) printf("%.3f]\n\n", gsl_matrix_get(m, i, j));
        else printf("%.3f\n", gsl_matrix_get(m, i, j));
      }
      else printf("%.3f ", gsl_matrix_get(m, i, j));
    }
  }
}

void randomize_mat(gsl_matrix* m, unsigned int seed){
  double val;
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < m->size2; j++){
      seed++;
      val = 10.0*((double) rand_r(&seed))/((double) RAND_MAX);
      gsl_matrix_set(m, i, j, val);
    }
  }
}

void randomize_vec(gsl_vector* v, unsigned int seed){
  double val;
  for(int i = 0; i < v->size; i++){
    seed++;
    val = 10.0*((double) rand_r(&seed))/((double) RAND_MAX);
    gsl_vector_set(v, i, val);
  }
}

double dot(gsl_vector* a, gsl_vector* b){
  assert(a->size == b->size);
  double res = 0;
  for(int i = 0; i < a->size; i++){
    res += gsl_vector_get(a, i)*gsl_vector_get(b, i);
  }
  return res;
}

void vector_sub_scaled(gsl_vector* a, gsl_vector* q, double s){
  double val;
  for(int i = 0; i < a->size; i++){
    val = gsl_vector_get(a, i) - gsl_vector_get(q, i)*s;
    gsl_vector_set(a, i, val);
  }
}

void normalize_collums(gsl_matrix* m){
  double scale;
  for(int i = 0; i < m->size2; i++){
    gsl_vector_view a_view = gsl_matrix_column(m, i);
    gsl_vector* a_i = &a_view.vector;
    scale = 1.0/sqrt(dot(a_i, a_i));
    gsl_vector_scale(a_i, scale);
  }
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
  gsl_vector* q = gsl_vector_alloc(A->size1);
  for(int i = 0; i < A->size2; i++){
    gsl_matrix_get_col(q, A, i);
    gsl_matrix_set(R, i, i, sqrt(dot(q, q)));
    double scale = 1.0/(gsl_matrix_get(R, i, i));
    gsl_vector_scale(q, scale);
    for(int j = i+1; j < A->size2; j++){
      gsl_vector_view a_view = gsl_matrix_column(A, j);
      gsl_vector* a_j = &a_view.vector;
      gsl_matrix_set(R, i, j, dot(q, a_j));
      vector_sub_scaled(a_j, q, gsl_matrix_get(R, i, j));
    }
  }
  normalize_collums(A);
  gsl_vector_free(q);
}

void backsub(const gsl_matrix* U, gsl_vector* c){
  for(int i = c->size-1; i>=0; i--){
    double s = gsl_vector_get(c, i);
    for(int k = i + 1; k < c->size; k++){
      s -= gsl_matrix_get(U, i, k)*gsl_vector_get(c, k);
    }
    gsl_vector_set(c, i, s/gsl_matrix_get(U, i, i));
  }
}

void backsub_bidiagonal(const gsl_matrix* U, gsl_vector* c){
  for(int i = c->size-1; i>=0; i--){
    double s = gsl_vector_get(c, i);
    if(i != c->size-1){
      s -= gsl_matrix_get(U, i, i+1)*gsl_vector_get(c, i+1);
    }
    gsl_vector_set(c, i, s/gsl_matrix_get(U, i, i));
  }
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x){
  gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
  backsub(R, x);
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
  gsl_matrix_set_identity(B);
  gsl_vector* copy = gsl_vector_alloc(B->size1);
  for(int i = 0; i < Q->size1; i++){
    gsl_matrix_get_col(copy, B, i);
    gsl_vector_view v = gsl_matrix_column(B, i);
    qr_gs_solve(Q, R, copy, &v.vector);
  }
  gsl_vector_free(copy);
}

void GKLBP(const gsl_matrix* A, gsl_matrix* U, gsl_matrix* V, gsl_matrix* B){
  gsl_matrix_set_identity(V);
  for(int k = 0; k < A->size1; k++){
    gsl_vector_view v_k = gsl_matrix_column(V, k);
    gsl_vector_view u_k = gsl_matrix_column(U, k);
    gsl_blas_dgemv(CblasNoTrans, 1.0, A, &v_k.vector, 0.0, &u_k.vector);
    if(k != 0){
      gsl_vector* u_k_minus = gsl_vector_alloc(A->size1);
      gsl_matrix_get_col(u_k_minus, U, k-1);
      gsl_vector_scale(u_k_minus, gsl_matrix_get(B, k-1, k));
      gsl_vector_sub(&u_k.vector, u_k_minus);
      gsl_vector_free(u_k_minus);
    }
    double alpha_k = sqrt(dot(&u_k.vector, &u_k.vector));
    gsl_matrix_set(B, k, k, alpha_k);
    gsl_vector_scale(&u_k.vector, 1.0/alpha_k);
    gsl_vector* v_k_copy = gsl_vector_alloc(A->size1);
    gsl_matrix_get_col(v_k_copy, V, k);
    gsl_vector_scale(v_k_copy, alpha_k);
    gsl_vector* v_k_plus = gsl_vector_alloc(A->size1);
    gsl_blas_dgemv(CblasTrans, 1.0, A, &u_k.vector, 0.0, v_k_plus);
    gsl_vector_sub(v_k_plus, v_k_copy);
    gsl_vector_free(v_k_copy);
    double beta_k = sqrt(dot(v_k_plus, v_k_plus));
    if(k != A->size1-1){gsl_matrix_set(B, k, k+1, beta_k);}
    gsl_vector_scale(v_k_plus, 1.0/beta_k);
    if (k != A->size1-1) {gsl_matrix_set_col(V, k+1, v_k_plus);}
    gsl_vector_free(v_k_plus);
  }
}

void GKLBP_solve(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, const gsl_vector* b, gsl_vector* x){
  gsl_vector* y = gsl_vector_alloc(B->size1);
  gsl_blas_dgemv(CblasTrans, 1.0, U, b, 0.0, y);
  backsub_bidiagonal(B, y);
  gsl_blas_dgemv(CblasNoTrans, 1.0, V, y, 0.0, x);
}

void GKLBP_inverse(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, gsl_matrix* Inv){
  gsl_matrix_set_identity(Inv);
  gsl_vector* copy = gsl_vector_alloc(Inv->size1);
  for(int i = 0; i < Inv->size1; i++){
    gsl_matrix_get_col(copy, Inv, i);
    gsl_vector_view v = gsl_matrix_column(Inv, i);
    GKLBP_solve(B, U, V, copy, &v.vector);
  }
  gsl_vector_free(copy);
}

double determinant(const gsl_matrix* B){
  double determinant = 1;
  for(int i = 0; i < B->size1; i++){
    determinant *= gsl_matrix_get(B, i, i);
  }
  return determinant;
}

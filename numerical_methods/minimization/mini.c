#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"mini.h"
#include<gsl/gsl_blas.h>
#include<float.h>


void print_vector(const gsl_vector* v){
  printf("[");
  for(int i = 0; i < v->size; i++){
    if (i != v->size-1) printf("%.20f\n", gsl_vector_get(v, i));
    else printf("%.20f]\n\n", gsl_vector_get(v, i));
  }
}

void print_matrix(const gsl_matrix* m){
  printf("[");
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < m->size2; j++){
      if (j == m->size2-1){
        if (i == m->size1-1) printf("%.3f]\n\n", gsl_matrix_get(m, i, j));
        else printf("%.3f\n", gsl_matrix_get(m, i, j));
      }
      else printf("%.3f ", gsl_matrix_get(m, i, j));
    }
  }
}

double dot(gsl_vector* a, gsl_vector* b){
  assert(a->size == b->size);
  double res = 0;
  for(int i = 0; i < a->size; i++){
    res += gsl_vector_get(a, i)*gsl_vector_get(b, i);
  }
  return res;
}

void vector_sub_scaled(gsl_vector* a, gsl_vector* q, double s){
  double val;
  for(int i = 0; i < a->size; i++){
    val = gsl_vector_get(a, i) - gsl_vector_get(q, i)*s;
    gsl_vector_set(a, i, val);
  }
}

void normalize_collums(gsl_matrix* m){
  double scale;
  for(int i = 0; i < m->size2; i++){
    gsl_vector_view a_view = gsl_matrix_column(m, i);
    gsl_vector* a_i = &a_view.vector;
    scale = 1.0/sqrt(dot(a_i, a_i));
    gsl_vector_scale(a_i, scale);
  }
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
  gsl_vector* q = gsl_vector_alloc(A->size1);
  for(int i = 0; i < A->size2; i++){
    gsl_matrix_get_col(q, A, i);
    gsl_matrix_set(R, i, i, sqrt(dot(q, q)));
    double scale = 1.0/(gsl_matrix_get(R, i, i));
    gsl_vector_scale(q, scale);
    for(int j = i+1; j < A->size2; j++){
      gsl_vector_view a_view = gsl_matrix_column(A, j);
      gsl_vector* a_j = &a_view.vector;
      gsl_matrix_set(R, i, j, dot(q, a_j));
      vector_sub_scaled(a_j, q, gsl_matrix_get(R, i, j));
    }
  }
  normalize_collums(A);
  gsl_vector_free(q);
}

void backsub(const gsl_matrix* U, gsl_vector* c){
  for(int i = c->size-1; i>=0; i--){
    double s = gsl_vector_get(c, i);
    for(int k = i + 1; k < c->size; k++){
      s -= gsl_matrix_get(U, i, k)*gsl_vector_get(c, k);
    }
    gsl_vector_set(c, i, s/gsl_matrix_get(U, i, i));
  }
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x){
  gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
  backsub(R, x);
}

void newton(double (*f)(gsl_vector*, gsl_vector*, gsl_matrix*), gsl_vector* x, double eps, step_data* step_d){
  int steps = 0;
  int calls = 0;
  double fx, fx_step, alpha, lambda, stdfdx;
  alpha = 0.0001;
  gsl_vector* dx = gsl_vector_alloc(x->size);
  gsl_vector* dfdx = gsl_vector_alloc(x->size);
  gsl_vector* dfdx_step = gsl_vector_alloc(x->size);
  gsl_matrix* H = gsl_matrix_alloc(x->size, x->size);
  gsl_matrix* R = gsl_matrix_calloc(x->size, x->size);
  fx = f(x, dfdx, H);
  while(sqrt(dot(dfdx, dfdx) > eps)){
    fx_step = fx;
    gsl_vector_memcpy(dfdx_step, dfdx);
    qr_gs_decomp(H, R);
    gsl_vector_scale(dfdx, -1.0);
    qr_gs_solve(H, R, dfdx, dx);
    gsl_vector_scale(dfdx, -1.0);
    gsl_vector_add(x, dx);
    fx = f(x, dfdx, H);
    calls++;
    steps++;
    gsl_vector_sub(x, dx);
    lambda = 1;
    stdfdx = dot(dx, dfdx_step);
    while(fx > fx_step + alpha*stdfdx && lambda > 1.0/64.0){
      lambda /= 2;
      gsl_vector_scale(dx, 0.5);
      gsl_vector_add(x, dx);
      fx = f(x, dfdx, H);
      calls++;
      stdfdx = dot(dx, dfdx_step);
      gsl_vector_sub(x, dx);
    }
    gsl_vector_add(x, dx);
  }
  gsl_vector_free(dx);
  gsl_vector_free(dfdx);
  gsl_vector_free(dfdx_step);
  gsl_matrix_free(H);
  gsl_matrix_free(R);
  step_d->steps = steps;
  step_d->calls = calls;
}

void numerical_gradient(double (*f)(gsl_vector*), gsl_vector* x, gsl_vector* df){
  gsl_vector* x_plus_dx = gsl_vector_alloc(x->size);
  gsl_vector_memcpy(x_plus_dx, x);
  double fdx, xi, dx;
  double fx = f(x);
  for(int i = 0; i < x->size; i++){
    xi = gsl_vector_get(x, i);
    //Setting step-size as in gsl_multiroot_fsolver_dnewton
		if (fabs(xi) > sqrt(DBL_EPSILON)) {
			dx = sqrt(DBL_EPSILON)*fabs(xi);
		}
    else {
			dx = DBL_EPSILON;
		}
    gsl_vector_set(x_plus_dx, i, xi + dx);
    fdx = f(x_plus_dx);
    gsl_vector_set(df, i, (fdx-fx)/dx);
    gsl_vector_set(x_plus_dx, i, xi);
  }
  gsl_vector_free(x_plus_dx);
}


void quasi_newton_broyden_symmetric(double (*f)(gsl_vector*), gsl_vector* x, double eps, step_data* step_d){
  int steps = 0;
  int calls = 0;
  int warnings = 0;
  double fx, fx_step, alpha, lambda, gamma, dotsy, stdfdx;
  alpha = 0.0001;
  gsl_vector* dx = gsl_vector_alloc(x->size);
  gsl_vector* dfdx = gsl_vector_alloc(x->size);
  gsl_vector* dfdx_step = gsl_vector_alloc(x->size);
  gsl_matrix* B = gsl_matrix_alloc(x->size, x->size);
  gsl_vector* y = gsl_vector_alloc(x->size);
  gsl_vector* a = gsl_vector_alloc(x->size);
  gsl_matrix_set_identity(B);
  numerical_gradient(f, x, dfdx);
  fx = f(x);
  while(sqrt(dot(dfdx, dfdx) > eps)){
    fx_step = fx;
    gsl_vector_memcpy(dfdx_step, dfdx);
    gsl_blas_dgemv(CblasNoTrans, -1.0, B, dfdx, 0.0, dx);
    gsl_vector_add(x, dx);
    fx = f(x);
    calls++;
    steps++;
    gsl_vector_sub(x, dx);
    lambda = 1;
    stdfdx = dot(dx, dfdx_step);
    while(fx >= fx_step + alpha*stdfdx && lambda > 1.0/64.0){
      lambda /= 2;
      gsl_vector_scale(dx, 0.5);
      gsl_vector_add(x, dx);
      fx = f(x);
      stdfdx = dot(dx, dfdx_step);
      calls++;
      gsl_vector_sub(x, dx);
      if (lambda == LAMBDA_MIN) {
				warnings++;
			}
    }
    gsl_vector_add(x, dx);
    numerical_gradient(f, x, dfdx);
    gsl_vector_memcpy(y, dfdx);
    gsl_vector_sub(y, dfdx_step);
    dotsy = dot(dx, y);
      if(fabs(dotsy) > 1e-6 && warnings < 2){
        gsl_blas_dgemv(CblasNoTrans, -1.0, B, y, 0.0, a);
        gsl_blas_daxpy(1.0, dx, a);
        gamma = dot(a, y)/(2*dotsy);
        gsl_blas_daxpy(-gamma, dx, a);
        gsl_blas_dger(1.0/dotsy, a, dx, B);
        gsl_blas_dger(1.0/dotsy, dx, a, B);
      }
    else{
      gsl_matrix_set_identity(B);
      warnings = 0;
    }
  }
  gsl_vector_free(dx);
  gsl_vector_free(dfdx);
  gsl_vector_free(dfdx_step);
  gsl_matrix_free(B);
  gsl_vector_free(y);
  gsl_vector_free(a);
  step_d->steps = steps;
  step_d->calls = calls;
}

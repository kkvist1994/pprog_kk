#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"mini.h"
#include<gsl/gsl_blas.h>

double rosen(gsl_vector* v, gsl_vector* dfdx, gsl_matrix* H){
  double x = gsl_vector_get(v, 0);
  double y = gsl_vector_get(v, 1);
  gsl_vector_set(dfdx, 0, -2*(1-x)-400*x*(y-x*x));
  gsl_vector_set(dfdx, 1, 200*(y-x*x));
  gsl_matrix_set(H, 0, 0, 2 + 1200*x*x - 400*y);
  gsl_matrix_set(H, 0, 1, -400*x);
  gsl_matrix_set(H, 1, 0, -400*x);
  gsl_matrix_set(H, 1, 1, 200);
  return pow(1-x, 2)+100*pow(y-x*x, 2);
}

double himmel(gsl_vector* v, gsl_vector* dfdx, gsl_matrix* H){
  double x = gsl_vector_get(v, 0);
  double y = gsl_vector_get(v, 1);
  gsl_vector_set(dfdx, 0, 4*x*(x*x+y-11)+2*(x+y*y-7));
  gsl_vector_set(dfdx, 1, 2*(x*x+y-11)+4*y*(x+y*y-7));
  gsl_matrix_set(H, 0, 0, 12*x*x+4*y-42);
  gsl_matrix_set(H, 0, 1, 4*x+4*y);
  gsl_matrix_set(H, 1, 0, 4*x+4*y);
  gsl_matrix_set(H,1, 1, 12*y*y+4*x-26);
  return pow(x*x+y-11, 2) + pow(x+y*y-7, 2);
}

double rosen_no_grad(gsl_vector* v){
  double x = gsl_vector_get(v, 0);
  double y = gsl_vector_get(v, 1);
  return pow(1-x, 2)+100*pow(y-x*x, 2);
}

double himmel_no_grad(gsl_vector* v){
  double x = gsl_vector_get(v, 0);
  double y = gsl_vector_get(v, 1);
  return pow(x*x+y-11, 2) + pow(x+y*y-7, 2);
}

double exp_fit(gsl_vector* p, double t){
  double A = gsl_vector_get(p, 0);
  double T = gsl_vector_get(p, 1);
  double B = gsl_vector_get(p, 2);
  return A*exp(-t/T)+B;
}

double least_squares_fit(gsl_vector* v){
  double square_sum = 0;
  double fx, square;
  double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
  double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
  double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
  int N = sizeof(t)/sizeof(t[0]);
  for(int i = 0; i < N; i++){
    fx = exp_fit(v, t[i]);
    square = pow(fx - y[i], 2);
    square_sum += square/(pow(e[i], 2));
  }
  return square_sum;
}

void fit_plot(double x_min, double x_max, double dx, double eps, step_data* s, gsl_vector* p){
  quasi_newton_broyden_symmetric(least_squares_fit, p, eps, s);
  double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
  double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
  double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
  int n = sizeof(t)/sizeof(t[0]);
  FILE* datastream = fopen("data.txt", "w");
  for(int i = 0; i < n; i++){
    fprintf(datastream, "%.5f %.5f %.5f\n", t[i], y[i], e[i]);
  }
  fclose(datastream);
  FILE* fitstream = fopen("fit.txt", "w");
  double x_fit = x_min;
  double fit;
  while(x_fit <= x_max){
    fit = exp_fit(p, x_fit);
    fprintf(fitstream, "%.5f %.5f\n", x_fit, fit);
    x_fit += dx;
  }
  fclose(fitstream);
  printf("The the obtained parameters are:\n");
  print_vector(p);
}

int main(void){
  gsl_vector* x = gsl_vector_alloc(2);
  gsl_vector_set(x, 0, 0.0);
  gsl_vector_set(x, 1, 3.0);
  double eps = 0.000001;
  step_data step_d = {0, 0};
  step_data* s = &step_d;
  printf("The minimum of the Rosenbrock valley function is found at the\n");
  printf("following coordinates using newtons method:\n");
  newton(rosen, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  printf("The minima of the Himmelblau function is found at the following four coordinates:\n");
  gsl_vector_set(x, 0, 3.2);
  gsl_vector_set(x, 1, 1.8);
  newton(himmel, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  gsl_vector_set(x, 0, -3.0);
  gsl_vector_set(x, 1, 3.0);
  newton(himmel, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  gsl_vector_set(x, 0, -4.0);
  gsl_vector_set(x, 1, -4.0);
  newton(himmel, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  gsl_vector_set(x, 0, 4.0);
  gsl_vector_set(x, 1, -2.0);
  newton(himmel, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  gsl_vector_set(x, 0, 0.0);
  gsl_vector_set(x, 1, 3.0);
  printf("The minimum of the Rosenbrock valley function is found at the\n");
  printf("following coordinates using a quasi newton method:\n");
  quasi_newton_broyden_symmetric(rosen_no_grad, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  printf("The minima of the Himmelblau function is found at the following four\n");
  printf("coordinates using a quasi newton method:\n");
  gsl_vector_set(x, 0, 3.2);
  gsl_vector_set(x, 1, 1.8);
  quasi_newton_broyden_symmetric(himmel_no_grad, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  gsl_vector_set(x, 0, -3.0);
  gsl_vector_set(x, 1, 3.0);
  quasi_newton_broyden_symmetric(himmel_no_grad, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  gsl_vector_set(x, 0, -4.0);
  gsl_vector_set(x, 1, -4.0);
  quasi_newton_broyden_symmetric(himmel_no_grad, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  gsl_vector_set(x, 0, 4.0);
  gsl_vector_set(x, 1, -2.0);
  quasi_newton_broyden_symmetric(himmel_no_grad, x, eps, s);
  print_vector(x);
  printf("The number of steps and function calls used to find this answer\n");
  printf("was %d and %d respectively\n\n", s->steps, s->calls);
  printf("Generally solving the equivalent problem by finding the roots of\n");
  printf("the derivatives uses, more steps and funtion calls, both compared\n");
  printf("to the newton and quasi newton method\n");
  gsl_vector_free(x);
  gsl_vector* p = gsl_vector_alloc(3);
  gsl_vector_set(p, 0, 5);
  gsl_vector_set(p, 1, 2);
  gsl_vector_set(p, 2, 1.5);
  fit_plot(0.01, 10, 0.01, eps, s, p);
  printf("Fitting the given data to an exponential function yields the\n");
  printf("following parameters:\n");
  print_vector(p);
  printf("The resulting graph can be seen in the file plot.svg\n");
  gsl_vector_free(p);
  return 0;
}

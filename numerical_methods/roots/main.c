#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"roots.h"
#include<gsl/gsl_blas.h>

void exercise_func(const gsl_vector* x_v, gsl_vector* fx, gsl_matrix* J){
  double A = 10000;
  double x = gsl_vector_get(x_v, 0);
  double y = gsl_vector_get(x_v, 1);
  gsl_vector_set(fx, 0, A*x*y-1);
  gsl_vector_set(fx, 1, exp(-x) + exp(-y) - 1.0 - 1.0/A);
  gsl_matrix_set(J, 0, 0, A*y);
  gsl_matrix_set(J, 0, 1, A*x);
  gsl_matrix_set(J, 1, 0, -exp(-x));
  gsl_matrix_set(J, 1, 1, -exp(-y));
}

void rosen(const gsl_vector* x_v, gsl_vector* fx, gsl_matrix* J){
  double x = gsl_vector_get(x_v, 0);
  double y = gsl_vector_get(x_v, 1);
  gsl_vector_set(fx, 0, -2*(1-x)-400*x*(y-x*x));
  gsl_vector_set(fx, 1, 200*(y-x*x));
  gsl_matrix_set(J, 0, 0, 2 + 1200*x*x - 400*y);
  gsl_matrix_set(J, 0, 1, -400*x);
  gsl_matrix_set(J, 1, 0, -400*x);
  gsl_matrix_set(J, 1, 1, 200);
}

void himmel(const gsl_vector* x_v, gsl_vector* fx, gsl_matrix* J){
  double x = gsl_vector_get(x_v, 0);
  double y = gsl_vector_get(x_v, 1);
  gsl_vector_set(fx, 0, 4*x*(x*x+y-11)+2*(x+y*y-7));
  gsl_vector_set(fx, 1, 2*(x*x+y-11)+4*y*(x+y*y-7));
  gsl_matrix_set(J, 0, 0, 12*x*x+4*y-42);
  gsl_matrix_set(J, 0, 1, 4*x+4*y);
  gsl_matrix_set(J, 1, 0, 4*x+4*y);
  gsl_matrix_set(J, 1, 1, 12*y*y+4*x-26);
}

void exercise_func_no_J(const gsl_vector* x_v, gsl_vector* fx){
  double A = 10000;
  double x = gsl_vector_get(x_v, 0);
  double y = gsl_vector_get(x_v, 1);
  gsl_vector_set(fx, 0, A*x*y-1);
  gsl_vector_set(fx, 1, exp(-x) + exp(-y) - 1.0 - 1.0/A);
}

void rosen_no_J(const gsl_vector* x_v, gsl_vector* fx){
  double x = gsl_vector_get(x_v, 0);
  double y = gsl_vector_get(x_v, 1);
  gsl_vector_set(fx, 0, -2*(1-x)-400*x*(y-x*x));
  gsl_vector_set(fx, 1, 200*(y-x*x));
}

void himmel_no_J(const gsl_vector* x_v, gsl_vector* fx){
  double x = gsl_vector_get(x_v, 0);
  double y = gsl_vector_get(x_v, 1);
  gsl_vector_set(fx, 0, 4*x*(x*x+y-11)+2*(x+y*y-7));
  gsl_vector_set(fx, 1, 2*(x*x+y-11)+4*y*(x+y*y-7));
}

int main(void){
  int iter;
  void* params = NULL;
  step_data step_d = {0, 0};
  step_data* s = &step_d;
  gsl_vector* root = gsl_vector_alloc(2);
  gsl_vector_set(root, 0, 1.0);
  gsl_vector_set(root, 1, 0.0);
  printf("Hello\n");
  newton_with_jacobian(exercise_func, root, 0.00001, s);
  printf("The root of the exercise function is:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, 1.0);
  gsl_vector_set(root, 1, 0.0);
  newton(exercise_func_no_J, root, 0.000001, 0.00001, s);
  printf("The root of the exercise function, found without an analytic jacobian is:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, 1.0);
  gsl_vector_set(root, 1, 0.0);
  newton_refined(exercise_func_no_J, root, 0.000001, 0.00001, s);
  printf("The minimum of the Rosenbrock Valley function found without an analytic jacobian an with refined linesearch is at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, 0.5);
  gsl_vector_set(root, 1, 0.2);
  newton_with_jacobian(rosen, root, 0.00001, s);
  printf("The minimum of the Rosenbrock Valley function is at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, 0.5);
  gsl_vector_set(root, 1, 0.2);
  newton_with_jacobian(rosen, root, 0.00001, s);
  printf("The minimum of the Rosenbrock Valley function is at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, 0.5);
  gsl_vector_set(root, 1, 0.2);
  iter = gsl_roots_with_jac(root, params, rosenbrock, rosenbrock_df, rosenbrock_fdf);
  printf("The number of steps used by the corresponding gsl routine was %d\n\n", iter);

  gsl_vector_set(root, 0, 0.5);
  gsl_vector_set(root, 1, 0.2);
  newton(rosen_no_J, root, 0.000001, 0.00001, s);
  printf("The minimum of the Rosenbrock Valley function found without an analytic jacobian is at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, 0.5);
  gsl_vector_set(root, 1, 0.2);
  iter = gsl_roots_no_jac(root, params, rosenbrock);
  printf("The number of steps used by the corresponding gsl routine was %d\n\n", iter);

  gsl_vector_set(root, 0, 0.5);
  gsl_vector_set(root, 1, 0.2);
  newton_refined(rosen_no_J, root, 0.000001, 0.00001, s);
  printf("The minimum of the Rosenbrock Valley function found without an analytic jacobian an with refined linesearch is at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, -6.0);
  gsl_vector_set(root, 1, -6.0);
  newton_with_jacobian(himmel, root, 0.00001, s);
  printf("One of the minima of the Himmelblau function is found at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, -6.0);
  gsl_vector_set(root, 1, -6.0);
  newton(himmel_no_J, root, 0.000001, 0.00001, s);
  printf("One minimum of the Himmelblau function found without an analytic jacobian is at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_set(root, 0, -6.0);
  gsl_vector_set(root, 1, -6.0);
  newton_refined(himmel_no_J, root, 0.000001, 0.00001, s);
  printf("The minimum of the Rosenbrock Valley function found without an analytic jacobian an with refined linesearch is at:\n");
  print_vector(root);
  printf("The number of steps and function calls used to find this answer was %d and %d respectively\n\n", s->steps, s->calls);

  gsl_vector_free(root);
  return 0;
}

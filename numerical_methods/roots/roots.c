#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"roots.h"
#include<gsl/gsl_blas.h>
#include<gsl/gsl_multiroots.h>

void print_vector(const gsl_vector* v){
  printf("[");
  for(int i = 0; i < v->size; i++){
    if (i != v->size-1) printf("%.10f\n", gsl_vector_get(v, i));
    else printf("%.10f]\n\n", gsl_vector_get(v, i));
  }
}

void print_matrix(const gsl_matrix* m){
  printf("[");
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < m->size2; j++){
      if (j == m->size2-1){
        if (i == m->size1-1) printf("%.3f]\n\n", gsl_matrix_get(m, i, j));
        else printf("%.3f\n", gsl_matrix_get(m, i, j));
      }
      else printf("%.3f ", gsl_matrix_get(m, i, j));
    }
  }
}

double dot(gsl_vector* a, gsl_vector* b){
  assert(a->size == b->size);
  double res = 0;
  for(int i = 0; i < a->size; i++){
    res += gsl_vector_get(a, i)*gsl_vector_get(b, i);
  }
  return res;
}

void vector_sub_scaled(gsl_vector* a, gsl_vector* q, double s){
  double val;
  for(int i = 0; i < a->size; i++){
    val = gsl_vector_get(a, i) - gsl_vector_get(q, i)*s;
    gsl_vector_set(a, i, val);
  }
}

void normalize_collums(gsl_matrix* m){
  double scale;
  for(int i = 0; i < m->size2; i++){
    gsl_vector_view a_view = gsl_matrix_column(m, i);
    gsl_vector* a_i = &a_view.vector;
    scale = 1.0/sqrt(dot(a_i, a_i));
    gsl_vector_scale(a_i, scale);
  }
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
  gsl_vector* q = gsl_vector_alloc(A->size1);
  for(int i = 0; i < A->size2; i++){
    gsl_matrix_get_col(q, A, i);
    gsl_matrix_set(R, i, i, sqrt(dot(q, q)));
    double scale = 1.0/(gsl_matrix_get(R, i, i));
    gsl_vector_scale(q, scale);
    for(int j = i+1; j < A->size2; j++){
      gsl_vector_view a_view = gsl_matrix_column(A, j);
      gsl_vector* a_j = &a_view.vector;
      gsl_matrix_set(R, i, j, dot(q, a_j));
      vector_sub_scaled(a_j, q, gsl_matrix_get(R, i, j));
    }
  }
  normalize_collums(A);
  gsl_vector_free(q);
}

void backsub(const gsl_matrix* U, gsl_vector* c){
  for(int i = c->size-1; i>=0; i--){
    double s = gsl_vector_get(c, i);
    for(int k = i + 1; k < c->size; k++){
      s -= gsl_matrix_get(U, i, k)*gsl_vector_get(c, k);
    }
    gsl_vector_set(c, i, s/gsl_matrix_get(U, i, i));
  }
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x){
  gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
  backsub(R, x);
}

void solve_lin_eq(gsl_matrix* A, gsl_vector* b, gsl_vector* x){
  gsl_matrix* R = gsl_matrix_alloc(A->size1, A->size2);
  qr_gs_decomp(A, R);
  qr_gs_solve(A, R, b, x);
  gsl_matrix_free(R);
}

void newton_with_jacobian(void (*f)(const gsl_vector*, gsl_vector*, gsl_matrix*), gsl_vector* x, double eps, step_data* step_d){
  int steps = 0;
  int calls = 0;
  gsl_vector* dx = gsl_vector_alloc(x->size);
  gsl_vector* fx = gsl_vector_alloc(x->size);
  gsl_matrix* J = gsl_matrix_alloc(x->size, x->size);
  f(x, fx, J);
  double fx_norm;
  double lambda;
  while(sqrt(dot(fx, fx)) > eps){
    fx_norm = sqrt(dot(fx, fx));
    gsl_vector_scale(fx, -1.0);
    solve_lin_eq(J, fx, dx);
    gsl_vector_add(x, dx);
    f(x, fx, J);
    steps++;
    calls++;
    gsl_vector_sub(x, dx);
    lambda = 1;
    while(sqrt(dot(fx, fx)) > (1-lambda/2)*fx_norm && lambda > 1.0/64.0){
      lambda /= 2;
      gsl_vector_scale(dx, 0.5);
      gsl_vector_add(x, dx);
      f(x, fx, J);
      calls++;
      gsl_vector_sub(x, dx);
    }
    gsl_vector_add(x, dx);
  }
  gsl_vector_free(dx);
  gsl_vector_free(fx);
  gsl_matrix_free(J);
  step_d->steps = steps;
  step_d->calls = calls;
}

void numerical_jacobian(void (*f)(const gsl_vector*, gsl_vector*), gsl_vector* x, gsl_vector* fx, double delta, gsl_matrix* J){
  gsl_vector* fx_delta = gsl_vector_alloc(J->size1);
  for(int j = 0; j<J->size2; j++){
    gsl_vector_set(x, j, gsl_vector_get(x, j)+delta);
    f(x, fx_delta);
    gsl_vector_set(x, j, gsl_vector_get(x, j)-delta);
    gsl_vector_sub(fx_delta, fx);
    gsl_vector_scale(fx_delta, 1.0/delta);
    gsl_matrix_set_col(J, j, fx_delta);
  }
  gsl_vector_free(fx_delta);
}

void newton(void (*f)(const gsl_vector*, gsl_vector*), gsl_vector* x, double delta, double eps, step_data* step_d){
  int steps = 0;
  int calls = 0;
  gsl_vector* dx = gsl_vector_alloc(x->size);
  gsl_vector* fx = gsl_vector_alloc(x->size);
  gsl_matrix* J = gsl_matrix_alloc(x->size, x->size);
  f(x, fx);
  numerical_jacobian(f, x, fx, delta, J);
  double fx_norm;
  double lambda;
  while(sqrt(dot(fx, fx)) > eps){
    fx_norm = sqrt(dot(fx, fx));
    gsl_vector_scale(fx, -1.0);
    solve_lin_eq(J, fx, dx);
    gsl_vector_add(x, dx);
    f(x, fx);
    numerical_jacobian(f, x, fx, delta, J);
    steps++;;
    calls++;
    gsl_vector_sub(x, dx);
    lambda = 1;
    while(sqrt(dot(fx, fx)) > (1-lambda/2)*fx_norm && lambda > 1.0/64.0){
      lambda /= 2;
      gsl_vector_scale(dx, 0.5);
      gsl_vector_add(x, dx);
      f(x, fx);
      numerical_jacobian(f, x, fx, delta, J);
      calls++;
      gsl_vector_sub(x, dx);
    }
    gsl_vector_add(x, dx);
  }
  gsl_vector_free(dx);
  gsl_vector_free(fx);
  gsl_matrix_free(J);
  step_d->steps = steps;
  step_d->calls = calls;
}

int rosenbrock(const gsl_vector* x_v, void* params, gsl_vector* f){
   const double x = gsl_vector_get (x_v, 0);
   const double y = gsl_vector_get (x_v, 1);
   gsl_vector_set(f, 0, -2*(1-x)-400*x*(y-x*x));
   gsl_vector_set(f, 1, 200*(y-x*x));
   return GSL_SUCCESS;
}

int rosenbrock_df(const gsl_vector* x_v, void* p, gsl_matrix* J){
  double x = gsl_vector_get(x_v, 0);
  double y = gsl_vector_get(x_v, 1);
  gsl_matrix_set(J, 0, 0, 2 + 1200*x*x - 400*y);
  gsl_matrix_set(J, 0, 1, -400*x);
  gsl_matrix_set(J, 1, 0, -400*x);
  gsl_matrix_set(J, 1, 1, 200);
  return GSL_SUCCESS;
}

int rosenbrock_fdf(const gsl_vector* x_v, void* p, gsl_vector* fx ,gsl_matrix* J){
  rosenbrock(x_v, p, fx);
  rosenbrock_df(x_v, p, J);
  return GSL_SUCCESS;
}

int gsl_roots_no_jac(gsl_vector* x, void* params, int (*func)(const gsl_vector*, void*, gsl_vector*)){
   const gsl_multiroot_fsolver_type *T;
   gsl_multiroot_fsolver *s;
   int status;
   const size_t n = 2;
   size_t iter = 0;
   gsl_multiroot_function f = {func, n, params};
   T = gsl_multiroot_fsolver_hybrids;
   s = gsl_multiroot_fsolver_alloc (T, 2);
   gsl_multiroot_fsolver_set (s, &f, x);
   do {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);
      if (status) {break;}
      status = gsl_multiroot_test_residual (s->f, 1e-5);
   }
   while (status == GSL_CONTINUE && iter < 1000);
   gsl_multiroot_fsolver_free (s);
   return iter;
}

int gsl_roots_with_jac(gsl_vector* x, void* params, int (*func_f)(const gsl_vector*, void*, gsl_vector*), int (*func_df)(const gsl_vector*, void*, gsl_matrix*), int (*func_fdf)(const gsl_vector*, void*, gsl_vector*, gsl_matrix*)){
   const gsl_multiroot_fdfsolver_type *T;
   gsl_multiroot_fdfsolver *s;
   int status;
   const size_t n = 2;
   size_t iter = 0;
   gsl_multiroot_function_fdf f = {func_f, func_df, func_fdf, n, params};
   T = gsl_multiroot_fdfsolver_gnewton;
   s = gsl_multiroot_fdfsolver_alloc (T, n);
   gsl_multiroot_fdfsolver_set (s, &f, x);
   do {
      iter++;
      status = gsl_multiroot_fdfsolver_iterate (s);
      if (status) {break;}
      status = gsl_multiroot_test_residual (s->f, 1e-5);
   }
   while (status == GSL_CONTINUE && iter < 1000);
   gsl_multiroot_fdfsolver_free (s);
   return iter;
}

void newton_refined(void (*f)(const gsl_vector*, gsl_vector*), gsl_vector* x, double delta, double eps, step_data* step_d){
  int steps = 0;
  int calls = 0;
  gsl_vector* dx = gsl_vector_alloc(x->size);
  gsl_vector* fx = gsl_vector_alloc(x->size);
  gsl_matrix* J = gsl_matrix_alloc(x->size, x->size);
  f(x, fx);
  numerical_jacobian(f, x, fx, delta, J);
  double fx_norm, lambda, g_0, g_p_0, g_trial, c;
  while(sqrt(dot(fx, fx)) > eps){
    fx_norm = sqrt(dot(fx, fx));
    g_0 = 0.5*fx_norm*fx_norm;
    g_p_0 = -fx_norm*fx_norm;
    gsl_vector_scale(fx, -1.0);
    solve_lin_eq(J, fx, dx);
    gsl_vector_add(x, dx);
    f(x, fx);
    numerical_jacobian(f, x, fx, delta, J);
    steps++;;
    calls++;
    gsl_vector_sub(x, dx);
    lambda = 1;
    g_trial = 0.5*dot(fx, fx);
    while(sqrt(dot(fx, fx)) > (1-lambda/2)*fx_norm && lambda > 1.0/64.0){
      g_trial = 0.5*dot(fx, fx);
      c = (g_trial-g_0-g_p_0*lambda)/(lambda*lambda);
      lambda = -g_p_0/(2.0*c);
      gsl_vector_scale(dx, lambda);
      gsl_vector_add(x, dx);
      f(x, fx);
      numerical_jacobian(f, x, fx, delta, J);
      calls++;
      gsl_vector_sub(x, dx);
      gsl_vector_scale(dx, 1/lambda);
    }
    gsl_vector_add(x, dx);
  }
  gsl_vector_free(dx);
  gsl_vector_free(fx);
  gsl_matrix_free(J);
  step_d->steps = steps;
  step_d->calls = calls;
}

typedef struct {int steps; int calls;} step_data;
void print_vector(const gsl_vector*);
void print_matrix(const gsl_matrix*);
void newton_with_jacobian(void (*)(const gsl_vector*, gsl_vector*, gsl_matrix*), gsl_vector*, double, step_data*);
void newton(void (*)(const gsl_vector*, gsl_vector*), gsl_vector*, double, double, step_data*);
void newton_refined(void (*)(const gsl_vector*, gsl_vector*), gsl_vector*, double, double, step_data*);
int gsl_roots_no_jac(gsl_vector*, void*, int (*)(const gsl_vector*, void*, gsl_vector*));
int rosenbrock(const gsl_vector*, void*, gsl_vector*);
int rosenbrock_df(const gsl_vector*, void*, gsl_matrix*);
int rosenbrock_fdf(const gsl_vector*, void*, gsl_vector*, gsl_matrix*);
int gsl_roots_with_jac(gsl_vector*, void*, int (*)(const gsl_vector*, void*, gsl_vector*), int (*)(const gsl_vector*, void*, gsl_matrix*), int (*)(const gsl_vector*, void*, gsl_vector*, gsl_matrix*));

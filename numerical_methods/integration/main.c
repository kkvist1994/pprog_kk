#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"integ.h"
#include<gsl/gsl_blas.h>
#include<gsl/gsl_integration.h>

double f1(double x){
  return sqrt(x);
}

double f2(double x){
  return 1.0/sqrt(x);
}

double f3(double x){
  return log(x)/sqrt(x);
}

double f4(double x){
  return 4*sqrt(1-pow((1-x), 2));
}

double f1_p(double x, void* p){
  return sqrt(x);
}

double f2_p(double x, void* p){
  return 1.0/sqrt(x);
}

double f3_p(double x, void* p){
  return log(x)/sqrt(x);
}

double f4_p(double x, void* p){
  return 4*sqrt(1-pow((1-x), 2));
}

int main(void){
  double err = 0, a = 0, b = 1, acc = 1e-10, eps = 1e-10;
  int nrec = 0, int_eval;
  double Q = integ_helper(f1, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(sqrt(x)) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);
  err = 0, nrec = 0;
  Q = integ_helper(f2, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(1/sqrt(x)) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);
  err = 0, nrec = 0;
  Q = integ_helper(f3, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(ln(x)/sqrt(x)) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);
  Q = integ_helper(f4, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(4*sqrt(1-pow((1-x), 2))) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("For comparison math.h defines pi as %.15f.\n", M_PI);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);

  printf("Now doing the same four claculations with variable transformation.\n\n");
  err = 0, nrec = 0;
  Q = clenshaw_curtis(f1, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(sqrt(x)) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);
  err = 0, nrec = 0;
  Q = clenshaw_curtis(f2, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(1.0/sqrt(x)) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);
  err = 0, nrec = 0;
  Q = clenshaw_curtis(f3, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(log(x)/sqrt(x)) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);
  Q = clenshaw_curtis(f4, a, b, acc, eps, &err, &nrec, &int_eval);
  printf("Calculating int(4*sqrt(1-pow((1-x), 2))) from 0 to 1 yields: %.15f, calculated with an error of approximately %.15f.\n", Q, err);
  printf("For comparison math.h defines pi as %.15f.\n", M_PI);
  printf("The number of recursive steps used to provide this answer was %d.\n", nrec);
  printf("While the number of integrand evaluations used to provide this answer was %d.\n\n", int_eval);
  printf("From the calculations above one can see that the variable transformation\n");
  printf("is slightly less efficient when the integrand does not diverge in the ends,\n");
  printf("but far more efficient when it does.\n");
  gsl_integration_workspace * w = gsl_integration_workspace_alloc(1000);
  /* Library method */
  double result, error;
  double *params = NULL;
  gsl_function F;
  F.function = &f1_p;
  F.params = params;
  printf("The library funtion used in the following uses Gauss-Kronrod 21 point integration\n");
  printf("Calculating int(sqrt(x)) using library functions with the following results:\n");
  gsl_integration_qags(&F, 0, 1, acc, eps, 1000, w, &result, &error);
  printf ("result          = % .15f\n", result);
  printf ("estimated error = % .15f\n", error);
  printf ("intervals       = %zu\n", w->size);
  F.function = &f2_p;
  printf("Calculating int(1.0/sqrt(x)) using library functions with the following results:\n");
  gsl_integration_qags(&F, 0, 1, acc, eps, 1000, w, &result, &error);
  printf ("result          = % .15f\n", result);
  printf ("estimated error = % .15f\n", error);
  printf ("intervals       = %zu\n", w->size);
  F.function = &f3_p;
  printf("Calculating int(log(x)/sqrt(x)) using library functions with the following results:\n");
  gsl_integration_qags(&F, 0, 1, acc, eps, 1000, w, &result, &error);
  printf ("result          = % .15f\n", result);
  printf ("estimated error = % .15f\n", error);
  printf ("intervals       = %zu\n", w->size);
  F.function = &f4_p;
  printf("Calculating int(4*sqrt(1-pow((1-x), 2))) using library functions with the following results:\n");
  gsl_integration_qags(&F, 0, 1, acc, eps, 1000, w, &result, &error);
  printf ("result          = % .15f\n", result);
  printf ("estimated error = % .15f\n", error);
  printf ("intervals       = %zu\n", w->size);
  printf("\nIn general the library functions are significantly better than\n");
  printf("the very simple integration routines I implemented here.\n");
  gsl_integration_workspace_free (w);
  return 0;
}

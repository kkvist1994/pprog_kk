The matrix A to diagonalize is:
[0.857 3.465 1.911 2.772 4.797
3.465 4.733 -4.078 -3.092 -3.044
1.911 -4.078 0.759 0.425 -3.954
2.772 -3.092 0.425 -0.713 4.021
4.797 -3.044 -3.954 4.021 -1.454]

Testing the decomposition achieved by the cyclic method:
V^TAV gives:
[-9.893 -0.000 0.000 -0.000 0.000
-0.000 -4.443 0.000 -0.000 -0.000
0.000 0.000 2.446 -0.000 0.000
-0.000 0.000 -0.000 7.271 -0.000
0.000 -0.000 0.000 -0.000 8.802]

For comparison D is:
[-9.893 -0.000 -0.000 -0.000 0.000
-0.000 -4.443 0.000 0.000 -0.000
-0.000 0.000 2.446 -0.000 0.000
-0.000 0.000 -0.000 7.271 -0.000
0.000 -0.000 0.000 -0.000 8.802]

The value by value method will find the smallest value first, 
since s*c*Apg is always positive, so we move value down the diagonal. 
The iteration over p is the outer loop in this algorith, so ones the first
Eigenvalue is found, the first row is zeroed.
Now since A'pi and A'qi only depends on values in their own row, these will keep being zero.
To find the next eigenvalue we keep moving value down the diagonal, and find the second smallest. 
To find the largest first we utilize that tan is pi periodic, so adding pi/2 to phi will result in the same angle, but change the sign og s*c, 
and in this way, the largest values are first in the trace og D.
Using value by value to find only the smallest eigenvalue, 
saved in the first entry of the vector e:
[-9.893
6.999
3.781
2.074
1.223]

Using value by value to find only the 3 largest eigenvalues, 
saved in the first three entries of the vector e:
[8.802
7.271
2.446
-7.348
-6.988]

The number of rotations used to find the smalles eigenvalue was 60. 
 In comparison finding all eigenvalues with the cyclic method took 50 roations.
The number of rotations used to diagonalize A using value by value
and finding the smallest eigenvalues first is: 95
The number of rotations used to diagonalize A using value by value
and finding the largest eigenvalues first is: 281
The number of rotations used by the classic method is: 34
Testing the decomposition achieved by the classic method:
V^TAV gives:
[-9.893 0.000 0.000 -0.000 -0.000
0.000 -4.443 -0.000 -0.000 -0.000
0.000 -0.000 2.446 -0.000 0.004
-0.000 -0.000 -0.000 7.271 -0.000
-0.000 -0.000 0.004 -0.000 8.802]

For comparison D is:
[-9.893 -0.000 0.000 0.000 -0.000
-0.000 -4.443 0.000 -0.000 -0.000
0.000 0.000 2.446 -0.000 0.004
0.000 -0.000 -0.000 7.271 -0.000
-0.000 -0.000 0.004 -0.000 8.802]


#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void randomize_symmetric(gsl_matrix*, unsigned int*);
int jacobi(gsl_matrix*, gsl_vector*, gsl_matrix*);
void print_matrix(const gsl_matrix*);
void print_vector(const gsl_vector*);
double diag_time(int, unsigned int*, int (*)(gsl_matrix*, gsl_vector*, gsl_matrix*));
void plot_complexity(int, unsigned int*);
int jacobi_min(gsl_matrix*, gsl_vector*, gsl_matrix*, int, int);
int classic_jacobi(gsl_matrix*, gsl_vector*, gsl_matrix*);

#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<stdio.h>
#include<stdlib.h>
#include"eigen.h"
#include<time.h>

void print_vector(const gsl_vector* v){
  printf("[");
  for(int i = 0; i < v->size; i++){
    if (i != v->size-1) printf("%.3f\n", gsl_vector_get(v, i));
    else printf("%.3f]\n\n", gsl_vector_get(v, i));
  }
}

void print_array(int* a, int n){
  printf("[");
  for(int i = 0; i < n; i++){
    if(i != n-1){printf("%d, ", a[i]);}
    else{printf("%d]\n", a[i]);}
  }
}

void print_matrix(const gsl_matrix* m){
  printf("[");
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < m->size2; j++){
      if (j == m->size2-1){
        if (i == m->size1-1) printf("%.3f]\n\n", gsl_matrix_get(m, i, j));
        else printf("%.3f\n", gsl_matrix_get(m, i, j));
      }
      else printf("%.3f ", gsl_matrix_get(m, i, j));
    }
  }
}

void randomize_symmetric(gsl_matrix* m, unsigned int* seed){
  double val;
  for(int i = 0; i < m->size1; i++){
    for(int j = i; j < m->size2; j++){
      val = 10.0*((double) rand_r(seed))/((double) RAND_MAX) - 5.0;
      gsl_matrix_set(m, i, j, val);
      if(i != j) {gsl_matrix_set(m, j, i, val);}
    }
  }
}

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
  int changed, sweeps = 0, n = A->size1;
  for(int i = 0; i < n; i++){
    gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
  }
  gsl_matrix_set_identity(V);
  do{
    changed = 0; sweeps++; int p, q;
    for(p = 0; p < n-1; p++){
      for(q = p + 1; q<n; q++){
        double app = gsl_vector_get(e, p);
        double aqq = gsl_vector_get(e, q);
        double apq = gsl_matrix_get(A, p, q);
        double phi = 0.5*atan2(2*apq, aqq-app);
        double c = cos(phi), s = sin(phi);
        double app1 = c*c*app - 2*s*c*apq + s*s*aqq;
        double aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
        if (app1 != app || aqq1 != aqq){
          changed = 1;
          gsl_vector_set(e, p, app1);
          gsl_vector_set(e, q, aqq1);
          gsl_matrix_set(A, p, q, 0.0);
          for(int i = 0; i < p; i++){
            double aip = gsl_matrix_get(A, i, p);
            double aiq = gsl_matrix_get(A, i, q);
            gsl_matrix_set(A, i, p, c*aip - s*aiq);
            gsl_matrix_set(A, i, q, c*aiq + s*aip);
          }
          for(int i = p+1; i < q; i++){
            double api = gsl_matrix_get(A, p, i);
            double aiq = gsl_matrix_get(A, i, q);
            gsl_matrix_set(A, p, i, c*api - s*aiq);
            gsl_matrix_set(A, i, q, c*aiq + s*api);
          }
          for(int i = q+1; i < n; i++){
            double api = gsl_matrix_get(A, p, i);
            double aqi = gsl_matrix_get(A, q, i);
            gsl_matrix_set(A, p, i, c*api - s*aqi);
            gsl_matrix_set(A, q, i, c*aqi + s*api);
          }
          for(int i = 0; i < n; i++){
            double vip = gsl_matrix_get(V, i, p);
            double viq = gsl_matrix_get(V, i ,q);
            gsl_matrix_set(V, i ,p, c*vip - s*viq);
            gsl_matrix_set(V, i, q, c*viq + s*vip);
          }
        }
      }
    }
  }
  while(changed != 0);
  return sweeps;
}


double diag_time(int n, unsigned int* seed, int (*f)(gsl_matrix*, gsl_vector*, gsl_matrix*)){
  gsl_matrix* A = gsl_matrix_alloc(n, n);
  randomize_symmetric(A, seed);
  gsl_matrix* V = gsl_matrix_alloc(n, n);
  gsl_vector* e = gsl_vector_alloc(n);
  clock_t begin = clock();
  f(A, e, V);
  clock_t end = clock();
  gsl_matrix_free(A);
  gsl_matrix_free(V);
  gsl_vector_free(e);
  return (double)(end - begin) / CLOCKS_PER_SEC;
}

int jacobi_min_all(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
  int rotations = jacobi_min(A, e, V, A->size1, 0);
  return rotations;
}

int jacobi_min(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int n_max, int max){
  int changed, n = A->size1, rotations = 1;
  for(int i = 0; i < n; i++){
    gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
  }
  gsl_matrix_set_identity(V);
    for(int p = 0; p < n_max; p++){
      do{changed = 0;
      for(int q = p + 1; q<n; q++){
        double app = gsl_vector_get(e, p);
        double aqq = gsl_vector_get(e, q);
        double apq = gsl_matrix_get(A, p, q);
        double phi = 0.5*atan2(2*apq, aqq-app);
        if (max){phi += M_PI/2;}
        double c = cos(phi), s = sin(phi);
        double app1 = c*c*app - 2*s*c*apq + s*s*aqq;
        double aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
        if (app1 != app){
          rotations++;
          changed = 1;
          gsl_vector_set(e, p, app1);
          gsl_vector_set(e, q, aqq1);
          gsl_matrix_set(A, p, q, 0.0);
          for(int i = p+1; i < q; i++){
            double api = gsl_matrix_get(A, p, i);
            double aiq = gsl_matrix_get(A, i, q);
            gsl_matrix_set(A, p, i, c*api - s*aiq);
            gsl_matrix_set(A, i, q, c*aiq + s*api);
          }
          for(int i = q+1; i < n; i++){
            double api = gsl_matrix_get(A, p, i);
            double aqi = gsl_matrix_get(A, q, i);
            gsl_matrix_set(A, p, i, c*api - s*aqi);
            gsl_matrix_set(A, q, i, c*aqi + s*api);
          }
          for(int i = 0; i < n; i++){
            double vip = gsl_matrix_get(V, i, p);
            double viq = gsl_matrix_get(V, i ,q);
            gsl_matrix_set(V, i ,p, c*vip - s*viq);
            gsl_matrix_set(V, i, q, c*viq + s*vip);
          }
        }
      }
    }
    while(changed != 0);
  }
  return rotations;
}

int classic_jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
  int changed, n = A->size1, rotations = 0;
  for(int i = 0; i < n; i++){
    gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
  }
  gsl_matrix_set_identity(V);
  int* row_max = (int*) calloc(n, sizeof(int));
  for(int i = 0; i < n-1; i++){
    row_max[i] = i+1;
  }
  for(int p = 0; p < n-1; p++){
    for(int q = p + 1; q < n; q++){
      if(fabs(gsl_matrix_get(A, p, q)) > fabs(gsl_matrix_get(A, p, row_max[p]))){
        row_max[p] = q;
      }
    }
  }
  int p, q;
  do{
    changed = 0;
    for(p = 0; p < n-1; p++){
      q = row_max[p];
        double app = gsl_vector_get(e, p);
        double aqq = gsl_vector_get(e, q);
        double apq = gsl_matrix_get(A, p, q);
        double phi = 0.5*atan2(2*apq, aqq-app);
        double c = cos(phi), s = sin(phi);
        double app1 = c*c*app - 2*s*c*apq + s*s*aqq;
        double aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
        if(app1 != app || aqq1 != aqq){
          changed = 1; rotations++;
          gsl_vector_set(e, p, app1);
          gsl_vector_set(e, q, aqq1);
          gsl_matrix_set(A, p, q, 0.0);
          for(int i = 0; i < p; i++){
            double aip = gsl_matrix_get(A, i, p);
            double aiq = gsl_matrix_get(A, i, q);
            gsl_matrix_set(A, i, p, c*aip - s*aiq);
            gsl_matrix_set(A, i, q, c*aiq + s*aip);
          }
          for(int i = p+1; i < q; i++){
            double api = gsl_matrix_get(A, p, i);
            double aiq = gsl_matrix_get(A, i, q);
            gsl_matrix_set(A, p, i, c*api - s*aiq);
            gsl_matrix_set(A, i, q, c*aiq + s*api);
          }
          for(int i = q+1; i < n; i++){
            double api = gsl_matrix_get(A, p, i);
            double aqi = gsl_matrix_get(A, q, i);
            gsl_matrix_set(A, p, i, c*api - s*aqi);
            gsl_matrix_set(A, q, i, c*aqi + s*api);
          }
          for(int i = 0; i < n; i++){
            double vip = gsl_matrix_get(V, i, p);
            double viq = gsl_matrix_get(V, i ,q);
            gsl_matrix_set(V, i ,p, c*vip - s*viq);
            gsl_matrix_set(V, i, q, c*viq + s*vip);
          }
          row_max[p] = p+1;
          for(int i = p+2; i < n-1; i++){
            if (fabs(gsl_matrix_get(A, p, row_max[p])) < fabs(gsl_matrix_get(A, p, i))){
              row_max[p] = i;
            }
          }
          row_max[q] = q+1;
          for(int i = q+2; i < n-1; i++){
            if (fabs(gsl_matrix_get(A, q, row_max[q])) < fabs(gsl_matrix_get(A, q, i))){
              row_max[q] = i;
            }
          }
          for(int i = 0; i < p-1; i++){
            if(fabs(gsl_matrix_get(A, i, p)) > fabs(gsl_matrix_get(A, i, row_max[i]))){
              row_max[i] = p;
            }
          }
          for(int i = 0; i < q-1; i++){
            if(fabs(gsl_matrix_get(A, i, q)) > fabs(gsl_matrix_get(A, i, row_max[i]))){
              row_max[i] = q;
            }
          }
        }
      }
    }while(changed != 0);
      free(row_max);
  return rotations;
}

void plot_complexity(int n_max, unsigned int* seed){
  FILE* mystream = fopen("comp.txt", "w");
  double time_cyclic, time_val, time_classic;
  for(int i = 2; i <= n_max; i++){
    time_cyclic = diag_time(i, seed, jacobi);
    time_val = diag_time(i, seed, jacobi_min_all);
    time_classic = diag_time(i, seed, classic_jacobi);
    fprintf(mystream, "%d %.5f %.5f %.5f\n", i, time_cyclic, time_val, time_classic);
  }
}

#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"eigen.h"

void test_decomposition(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
  int n = A->size1;
  gsl_matrix* A_orig = gsl_matrix_alloc(n, n);
  gsl_matrix* D = gsl_matrix_alloc(n, n);
  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      if(j > i){
        gsl_matrix_set(D, i, j, gsl_matrix_get(A, i, j));
        gsl_matrix_set(D, j, i, gsl_matrix_get(A, i, j));
      }
      else if (i > j){
        gsl_matrix_set(A_orig, i, j, gsl_matrix_get(A, i, j));
        gsl_matrix_set(A_orig, j, i, gsl_matrix_get(A, i, j));
      }
      else{
        gsl_matrix_set(A_orig, i, i, gsl_matrix_get(A, i, j));
        gsl_matrix_set(D, i, i, gsl_vector_get(e, i));
      }
    }
  }
  gsl_matrix* VTA = gsl_matrix_alloc(n, n);
  gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, V, A_orig, 0.0, VTA);
  gsl_matrix* VTAV = gsl_matrix_alloc(n, n);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, VTA, V, 0.0, VTAV);
  printf("V^TAV gives:\n");
  print_matrix(VTAV);
  printf("For comparison D is:\n");
  print_matrix(D);
  gsl_matrix_free(A_orig);
  gsl_matrix_free(D);
  gsl_matrix_free(VTA);
  gsl_matrix_free(VTAV);
}

int main(void){
  int n = 5;
  unsigned int seed = time(NULL);
  gsl_matrix* A = gsl_matrix_alloc(n, n);
  randomize_symmetric(A, &seed);
  gsl_matrix* A_min = gsl_matrix_alloc(n, n);
  gsl_matrix_memcpy(A_min, A);
  gsl_matrix* A_max = gsl_matrix_alloc(n, n);
  gsl_matrix_memcpy(A_max, A);
  gsl_matrix* A_val_min = gsl_matrix_alloc(n, n);
  gsl_matrix_memcpy(A_val_min, A);
  gsl_matrix* A_val_max = gsl_matrix_alloc(n, n);
  gsl_matrix_memcpy(A_val_max, A);
  gsl_matrix* A_classic = gsl_matrix_alloc(n, n);
  gsl_matrix_memcpy(A_classic, A);
  printf("The matrix A to diagonalize is:\n");
  print_matrix(A);

  gsl_matrix* V = gsl_matrix_alloc(n, n);
  gsl_vector* e = gsl_vector_alloc(n);
  gsl_matrix* V_max = gsl_matrix_alloc(n, n);
  gsl_vector* e_max = gsl_vector_alloc(n);
  gsl_matrix* V_min = gsl_matrix_alloc(n, n);
  gsl_vector* e_min = gsl_vector_alloc(n);
  gsl_matrix* V_val_min = gsl_matrix_alloc(n, n);
  gsl_vector* e_val_min = gsl_vector_alloc(n);
  gsl_matrix* V_val_max = gsl_matrix_alloc(n, n);
  gsl_vector* e_val_max = gsl_vector_alloc(n);
  gsl_matrix* V_classic = gsl_matrix_alloc(n, n);
  gsl_vector* e_classic = gsl_vector_alloc(n);

  int sweeps = jacobi(A, e, V);
  int rot_all = sweeps*((n*n-n)/2);
  printf("Testing the decomposition achieved by the cyclic method:\n");
  test_decomposition(A, e, V);
  printf("The value by value method will find the smallest value first, \n");
  printf("since s*c*Apg is always positive, so we move value down the diagonal. \n");
  printf("The iteration over p is the outer loop in this algorith, so ones the first\n");
  printf("Eigenvalue is found, the first row is zeroed.\n");
  printf("Now since A'pi and A'qi only depends on values in their own row, these will keep being zero.\n");
  printf("To find the next eigenvalue we keep moving value down the diagonal, and find the second smallest. \n");
  printf("To find the largest first we utilize that tan is pi periodic, so adding pi/2 to phi will result in the same angle, but change the sign og s*c, \n");
  printf("and in this way, the largest values are first in the trace og D.\n");
  printf("Using value by value to find only the smallest eigenvalue, \nsaved in the first entry of the vector e:\n");
  int rot_min = jacobi_min(A_min, e_min, V_min, 1, 0);
  print_vector(e_min);
  printf("Using value by value to find only the 3 largest eigenvalues, \nsaved in the first three entries of the vector e:\n");
  jacobi_min(A_max, e_max, V_max, 3, 1);
  print_vector(e_max);
  printf("The number of rotations used to find the smalles eigenvalue was %d. \n In comparison finding all eigenvalues with the cyclic method took %d roations.\n", rot_min, rot_all);
  int rot_val_min = jacobi_min(A_val_min, e_val_min, V_val_min, n, 0);
  int rot_val_max = jacobi_min(A_val_max, e_val_max, V_val_max, n, 1);
  printf("The number of rotations used to diagonalize A using value by value\nand finding the smallest eigenvalues first is: %d\n", rot_val_min);
  printf("The number of rotations used to diagonalize A using value by value\nand finding the largest eigenvalues first is: %d\n", rot_val_max);
  int rot_classic = classic_jacobi(A_classic, e_classic, V_classic);
  printf("The number of rotations used by the classic method is: %d\n", rot_classic);
  printf("Testing the decomposition achieved by the classic method:\n");
  test_decomposition(A_classic, e_classic, V_classic);

  gsl_matrix_free(A);
  gsl_matrix_free(V);
  gsl_vector_free(e);
  gsl_matrix_free(A_max);
  gsl_matrix_free(V_max);
  gsl_vector_free(e_max);
  gsl_matrix_free(A_min);
  gsl_matrix_free(V_min);
  gsl_vector_free(e_min);
  gsl_matrix_free(A_val_min);
  gsl_matrix_free(V_val_min);
  gsl_vector_free(e_val_min);
  gsl_matrix_free(A_val_max);
  gsl_matrix_free(V_val_max);
  gsl_vector_free(e_val_max);
  gsl_matrix_free(A_classic);
  gsl_matrix_free(V_classic);
  gsl_vector_free(e_classic);
  plot_complexity(70, &seed);
  return 0;
}

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"least.h"
#include<gsl/gsl_blas.h>

void fit_plot(double* x, double* y, double* dy, int f_max, int n, char* dataname, char* fitname, double (*funs)(int, double), double x_min, double x_max, double dx, int thin){
  gsl_vector* x_v = gsl_vector_alloc(n);
  gsl_vector* y_v = gsl_vector_alloc(n);
  gsl_vector* dy_v = gsl_vector_alloc(n);
  gsl_matrix* covar = gsl_matrix_alloc(f_max, f_max);
  for(int i = 0; i < n; i++){
    gsl_vector_set(x_v, i, x[i]);
    gsl_vector_set(y_v, i, y[i]);
    gsl_vector_set(dy_v, i, dy[i]);
  }
  gsl_vector* c = gsl_vector_alloc(f_max);
  if (thin){
    least_squares_thin(funs, f_max, x_v, y_v, dy_v, c, covar);
  }
  else{
    least_squares(funs, f_max, x_v, y_v, dy_v, c, covar);
  }
  FILE* datastream = fopen(dataname, "w");
  for(int i = 0; i < n; i++){
    fprintf(datastream, "%.5f %.5f %.5f\n", x[i], y[i], dy[i]);
  }
  fclose(datastream);
  FILE* fitstream = fopen(fitname, "w");
  double x_fit = x_min;
  double fit, epsx;
  while(x_fit <= x_max){
    fit = evaluate(f_max, x_fit, c, funs);
    epsx = dx_calc(covar, funs, x_fit);
    fprintf(fitstream, "%.5f %.5f %.5f %.5f\n", x_fit, fit, fit-epsx, fit+epsx);
    x_fit += dx;
  }
  fclose(fitstream);
  printf("The the obtained parameters are:\n");
  print_vector(c);
  printf("The covariance matrix of the fit is:\n");
  print_matrix(covar);
  gsl_vector_free(x_v);
  gsl_vector_free(y_v);
  gsl_vector_free(dy_v);
  gsl_vector_free(c);
  gsl_matrix_free(covar);
}

int main(void){
  printf("The data given in the exercise is fittet and shown in a figure.\n");
  printf("The fit is obtained thriugh QR decomposition\n");
  double x[] = {0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9};
  double y[] = {-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
  double dy[] = {1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};
  int n = 9;
  int f_max = 3;
  fit_plot(x, y, dy, f_max, n, "data.txt", "fit.txt", funs1, 0.1, 10, 0.1, 0);


  printf("The data used in Dimitris example is fittet and shown in a figure.\n");
  printf("The fit is obtained thriugh singular value decomposition\n");
  double x2[] = {0.100,0.145,0.211,0.307,0.447,0.649,0.944,1.372,1.995,2.900};
  double y2[] = {12.644,9.235,7.377,6.460,5.555,5.896,5.673,6.964,8.896,11.355};
  double dy2[] = {0.858,0.359,0.505,0.403,0.683,0.605,0.856,0.351,1.083,1.002};
  int n2 = 10;
  int f_max2 = 3;
  fit_plot(x2, y2, dy2, f_max2, n2, "data2.txt", "fit2.txt", funs2, 0.1, 3.5, 0.05, 1);
  return 0;
}

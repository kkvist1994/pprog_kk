double evaluate(int, double, gsl_vector*, double (*)(int, double));
void least_squares(double (*)(int, double), int, gsl_vector*, gsl_vector*, gsl_vector*, gsl_vector*, gsl_matrix*);
double funs1(int, double);
double funs2(int, double);
void print_matrix(const gsl_matrix*);
void print_vector(const gsl_vector*);
double dx_calc(gsl_matrix*, double (*)(int, double), double);
void least_squares_thin(double (*)(int, double), int, gsl_vector*, gsl_vector*, gsl_vector* , gsl_vector*, gsl_matrix*);

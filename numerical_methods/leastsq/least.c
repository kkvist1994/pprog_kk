#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"least.h"
#include<gsl/gsl_blas.h>

void print_vector(const gsl_vector* v){
  printf("[");
  for(int i = 0; i < v->size; i++){
    if (i != v->size-1) printf("%.3f\n", gsl_vector_get(v, i));
    else printf("%.3f]\n\n", gsl_vector_get(v, i));
  }
}

void print_matrix(const gsl_matrix* m){
  printf("[");
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < m->size2; j++){
      if (j == m->size2-1){
        if (i == m->size1-1) printf("%.3f]\n\n", gsl_matrix_get(m, i, j));
        else printf("%.3f\n", gsl_matrix_get(m, i, j));
      }
      else printf("%.3f ", gsl_matrix_get(m, i, j));
    }
  }
}

double dot(gsl_vector* a, gsl_vector* b){
  assert(a->size == b->size);
  double res = 0;
  for(int i = 0; i < a->size; i++){
    res += gsl_vector_get(a, i)*gsl_vector_get(b, i);
  }
  return res;
}

void vector_sub_scaled(gsl_vector* a, gsl_vector* q, double s){
  double val;
  for(int i = 0; i < a->size; i++){
    val = gsl_vector_get(a, i) - gsl_vector_get(q, i)*s;
    gsl_vector_set(a, i, val);
  }
}

void normalize_collums(gsl_matrix* m){
  double scale;
  for(int i = 0; i < m->size2; i++){
    gsl_vector_view a_view = gsl_matrix_column(m, i);
    gsl_vector* a_i = &a_view.vector;
    scale = 1.0/sqrt(dot(a_i, a_i));
    gsl_vector_scale(a_i, scale);
  }
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
  gsl_vector* q = gsl_vector_alloc(A->size1);
  for(int i = 0; i < A->size2; i++){
    gsl_matrix_get_col(q, A, i);
    gsl_matrix_set(R, i, i, sqrt(dot(q, q)));
    double scale = 1.0/(gsl_matrix_get(R, i, i));
    gsl_vector_scale(q, scale);
    for(int j = i+1; j < A->size2; j++){
      gsl_vector_view a_view = gsl_matrix_column(A, j);
      gsl_vector* a_j = &a_view.vector;
      gsl_matrix_set(R, i, j, dot(q, a_j));
      vector_sub_scaled(a_j, q, gsl_matrix_get(R, i, j));
    }
  }
  normalize_collums(A);
  gsl_vector_free(q);
}

void backsub(const gsl_matrix* U, gsl_vector* c){
  for(int i = c->size-1; i>=0; i--){
    double s = gsl_vector_get(c, i);
    for(int k = i + 1; k < c->size; k++){
      s -= gsl_matrix_get(U, i, k)*gsl_vector_get(c, k);
    }
    gsl_vector_set(c, i, s/gsl_matrix_get(U, i, i));
  }
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x){
  gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
  backsub(R, x);
}

double funs1(int i, double x){
   switch(i){
   case 0: return log(x); break;
   case 1: return 1.0;   break;
   case 2: return x;     break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

double funs2(int i, double x){
   switch(i){
   case 0: return 1.0/x; break;
   case 1: return 1.0;   break;
   case 2: return x;     break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

double evaluate(int f_maks, double x, gsl_vector* c, double (*funs)(int, double)){
  double res = 0;
  for(int i = 0; i < f_maks; i++){
    res += gsl_vector_get(c, i)*funs(i, x);
  }
  return res;
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
  gsl_matrix_set_identity(B);
  gsl_vector* copy = gsl_vector_alloc(B->size1);
  for(int i = 0; i < Q->size1; i++){
    gsl_matrix_get_col(copy, B, i);
    gsl_vector_view v = gsl_matrix_column(B, i);
    qr_gs_solve(Q, R, copy, &v.vector);
  }
  gsl_vector_free(copy);
}

void inverse(gsl_matrix* A, gsl_matrix* A_i){
  gsl_matrix* R = gsl_matrix_alloc(A->size2, A->size2);
  qr_gs_decomp(A, R);
  qr_gs_inverse(A, R, A_i);
  gsl_matrix_free(R);
}

void least_squares(double (*f)(int, double), int f_maks, gsl_vector* x, gsl_vector* y, gsl_vector* eps, gsl_vector* c, gsl_matrix* covar){
  gsl_matrix* A = gsl_matrix_alloc(x->size, f_maks);
  gsl_matrix* R = gsl_matrix_alloc(f_maks, f_maks);
  double f_x;
  for(int i = 0; i < A->size1; i++){
    for(int j = 0; j < A->size2; j++){
      f_x = f(j, gsl_vector_get(x, i));
      gsl_matrix_set(A, i, j, f_x/gsl_vector_get(eps, i));
    }
  }
  qr_gs_decomp(A, R);
  gsl_matrix* RTR = gsl_matrix_alloc(R->size1, R->size2);
  gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, R, R, 0.0, RTR);
  inverse(RTR, covar);
  gsl_vector_div(y, eps);
  qr_gs_solve(A, R, y, c);
}

double dx_calc(gsl_matrix* covar, double (*funs)(int, double), double x){
  double sum = 0;
  for(int i = 0; i < covar->size1; i++){
    for(int j = 0; j < covar->size1; j++){
      sum += funs(i,x)*gsl_matrix_get(covar, i, j)*funs(j, x);
    }
  }
  return sqrt(sum);
}

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
  int changed, sweeps = 0, n = A->size1;
  for(int i = 0; i < n; i++){
    gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
  }
  gsl_matrix_set_identity(V);
  do{
    changed = 0; sweeps++; int p, q;
    for(p = 0; p < n-1; p++){
      for(q = p + 1; q<n; q++){
        double app = gsl_vector_get(e, p);
        double aqq = gsl_vector_get(e, q);
        double apq = gsl_matrix_get(A, p, q);
        double phi = 0.5*atan2(2*apq, aqq-app);
        double c = cos(phi), s = sin(phi);
        double app1 = c*c*app - 2*s*c*apq + s*s*aqq;
        double aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
        if (app1 != app || aqq1 != aqq){
          changed = 1;
          gsl_vector_set(e, p, app1);
          gsl_vector_set(e, q, aqq1);
          gsl_matrix_set(A, p, q, 0.0);
          for(int i = 0; i < p; i++){
            double aip = gsl_matrix_get(A, i, p);
            double aiq = gsl_matrix_get(A, i, q);
            gsl_matrix_set(A, i, p, c*aip - s*aiq);
            gsl_matrix_set(A, i, q, c*aiq + s*aip);
          }
          for(int i = p+1; i < q; i++){
            double api = gsl_matrix_get(A, p, i);
            double aiq = gsl_matrix_get(A, i, q);
            gsl_matrix_set(A, p, i, c*api - s*aiq);
            gsl_matrix_set(A, i, q, c*aiq + s*api);
          }
          for(int i = q+1; i < n; i++){
            double api = gsl_matrix_get(A, p, i);
            double aqi = gsl_matrix_get(A, q, i);
            gsl_matrix_set(A, p, i, c*api - s*aqi);
            gsl_matrix_set(A, q, i, c*aqi + s*api);
          }
          for(int i = 0; i < n; i++){
            double vip = gsl_matrix_get(V, i, p);
            double viq = gsl_matrix_get(V, i ,q);
            gsl_matrix_set(V, i ,p, c*vip - s*viq);
            gsl_matrix_set(V, i, q, c*viq + s*vip);
          }
        }
      }
    }
  }
  while(changed != 0);
  return sweeps;
}

void singular_val_decomp(gsl_matrix* A, gsl_matrix* V, gsl_vector* e){
  gsl_matrix* ATA = gsl_matrix_alloc(A->size2, A->size2);
  gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A, A, 0.0, ATA);
  jacobi(ATA, e, V);
  gsl_matrix_free(ATA);
}

void solve_least_squares_thin(gsl_matrix* A, gsl_vector* y, gsl_vector* c, gsl_matrix* covar, gsl_vector* eps, int f_maks){
  gsl_matrix* V = gsl_matrix_alloc(f_maks, f_maks);
  gsl_vector* e = gsl_vector_alloc(f_maks);
  singular_val_decomp(A, V, e);
  gsl_matrix* S = gsl_matrix_calloc(f_maks, f_maks);
  gsl_matrix* D_minus = gsl_matrix_calloc(f_maks, f_maks);
  gsl_matrix* D_minus_sq = gsl_matrix_calloc(f_maks, f_maks);
  for(int i = 0; i < f_maks; i++){
    gsl_matrix_set(S, i, i, sqrt(gsl_vector_get(e, i)));
    gsl_matrix_set(D_minus, i, i, 1.0/gsl_vector_get(e, i));
    gsl_matrix_set(D_minus_sq, i, i, 1.0/sqrt(gsl_vector_get(e, i)));
  }
  gsl_matrix* VD_min = gsl_matrix_alloc(f_maks, f_maks);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, V, D_minus, 0.0, VD_min);
  gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, VD_min, V, 0.0, covar);
  gsl_matrix* U = gsl_matrix_alloc(A->size1, A->size2);
  gsl_matrix* AV = gsl_matrix_alloc(A->size1, V->size2);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, V, 0.0, AV);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, AV, D_minus_sq, 0.0, U);
  gsl_vector* y_sol = gsl_vector_alloc(S->size1);
  gsl_vector* UTb = gsl_vector_alloc(U->size2);
  gsl_vector_div(y, eps);
  gsl_blas_dgemv(CblasTrans, 1.0, U, y, 0.0, UTb);
  for(int i = 0; i < y_sol->size; i++){
    gsl_vector_set(y_sol, i, gsl_vector_get(UTb, i)/gsl_matrix_get(S, i, i));
  }
  gsl_blas_dgemv(CblasNoTrans, 1.0, V, y_sol, 0.0, c);
  gsl_matrix_free(V);
  gsl_matrix_free(S);
  gsl_matrix_free(D_minus_sq);
  gsl_matrix_free(U);;
  gsl_vector_free(e);
  gsl_matrix_free(AV);
  gsl_matrix_free(D_minus);
}

void least_squares_thin(double (*f)(int, double), int f_maks, gsl_vector* x, gsl_vector* y, gsl_vector* eps, gsl_vector* c, gsl_matrix* covar){
  gsl_matrix* A = gsl_matrix_alloc(x->size, f_maks);
  double f_x;
  for(int i = 0; i < A->size1; i++){
    for(int j = 0; j < A->size2; j++){
      f_x = f(j, gsl_vector_get(x, i));
      gsl_matrix_set(A, i, j, f_x/gsl_vector_get(eps, i));
    }
  }
  solve_least_squares_thin(A, y, c, covar, eps, f_maks);
  gsl_matrix_free(A);
}

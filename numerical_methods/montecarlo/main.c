#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"mc.h"
#include<gsl/gsl_blas.h>
#include<time.h>

double f_test(gsl_vector* v){
  assert(v->size == 2);
  double x = gsl_vector_get(v, 0);
  double y = gsl_vector_get(v, 1);
  return exp(-(pow(x, 2) + pow(y,2)));
}

double f_singular(gsl_vector* v){
  assert(v->size == 3);
  double x = gsl_vector_get(v, 0);
  double y = gsl_vector_get(v, 1);
  double z = gsl_vector_get(v, 2);
  return pow(1-cos(x)*cos(y)*cos(z), -1)/pow(M_PI, 3);
}

int main(void){
  /*Produce different random numbers without recompiling*/
  unsigned int seed = time(NULL);
  srand(seed);
  gsl_vector* a = gsl_vector_alloc(2);
  gsl_vector_set(a, 0, -1);
  gsl_vector_set(a, 1, -1);
  gsl_vector* b = gsl_vector_alloc(2);
  gsl_vector_set(b, 0, 1);
  gsl_vector_set(b, 1, 1);
  double res, err;
  plainmc(a, b, f_test, 10000, &res, &err);
  printf("First testing with a simple the simple function exp(-(pow(x, 2) + pow(y,2)))\n");
  printf("Integrating exp(-(pow(x, 2) + pow(y,2))) with plain monte carlo integration\n");
  printf("from x = -1, y = -1 to x = 1, y = 1\n");
  printf("using 10000 sampling points yields %.15f\n", res);
  printf("with an estimated error of %.15f\n", err);
  printf("The exact result to a precision of 15 digits is 2.230985141404134\n\n");

  double aerr;
  int N = 10000;
  FILE* datastream = fopen("data.txt", "w");
  while(N<=1000000){
    plainmc(a, b, f_test, N, &res, &err);
    aerr = fabs(res-2.230985141404134);
    fprintf(datastream, "%d\t%.5f\t%.5f\t\n", N, err, aerr);
    N+=10000;
  }
  fclose(datastream);

  gsl_vector_free(a);
  gsl_vector_free(b);
  printf("Next i will do the integral given in the exercise\n");
  gsl_vector* a2 = gsl_vector_alloc(3);
  gsl_vector_set(a2, 0, 0);
  gsl_vector_set(a2, 1, 0);
  gsl_vector_set(a2, 2, 0);
  gsl_vector* b2 = gsl_vector_alloc(3);
  gsl_vector_set(b2, 0, M_PI);
  gsl_vector_set(b2, 1, M_PI);
  gsl_vector_set(b2, 2, M_PI);
  plainmc(a2, b2, f_singular, 1000000, &res, &err);
  printf("Integrating pow(1-cos(x)*cos(y)*cos(z), -1)/pow(M_PI, 3); with plain monte carlo integration\n");
  printf("from x = 0, y = 0, z = 0 to x = pi, y = pi, z = pi\n");
  printf("using 1000000 sampling points yields %.15f\n", res);
  printf("with an estimated error of %.15f\n", err);
  printf("The exact result to a precision of 15 digits is 1.393203929685676\n\n");

  printf("The figure in the file plot.svg shows that the actual error follows\n");
  printf("the trend of the estimated error nicely, and that the both follow the\n");
  printf("The shape of a O(1/sqrt(N)) function");

  gsl_vector_free(a2);
  gsl_vector_free(b2);
  return 0;
}

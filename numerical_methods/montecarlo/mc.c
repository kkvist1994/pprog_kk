#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"mc.h"
#include<gsl/gsl_blas.h>
#define RND ((double) rand()/RAND_MAX)

void random_v(gsl_vector* a, gsl_vector* b, gsl_vector* x){
  for(int i = 0; i<x->size; i++){
    gsl_vector_set(x, i, gsl_vector_get(a, i)
                         + RND*(gsl_vector_get(b,i)
                         - gsl_vector_get(a,i)));
  }
}

void plainmc(
  gsl_vector* a, gsl_vector* b, double f(gsl_vector* x), int N,
  double* res, double* err)
  {
    int i, dim = a->size;
    gsl_vector* x = gsl_vector_alloc(dim);
    double V = 1, sum = 0, sum2 = 0, fx;
    for(i = 0; i < dim; i++){
      V*=gsl_vector_get(b, i) - gsl_vector_get(a,i);
    }
    for(i = 0; i < N; i++){
      random_v(a, b, x);
      fx = f(x);
      sum+=fx;
      sum2+=pow(fx, 2);
    }
    double avr = sum/N, var = sum2/N-pow(avr, 2);
    *res = V*avr;
    *err = sqrt(var/N)*V;
    gsl_vector_free(x);
  }

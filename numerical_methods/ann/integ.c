#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"integ.h"
#include<gsl/gsl_blas.h>

/*The following function implements a recursive adaptive integrator with the same
  weights and evalutation points as in the example in the note. Also the implementation
  has found string inspiration from this example*/

double adapt_integ(double f(double), double a, double b, double acc, double eps, double f2, double f3, double* err, int* nrec, int* int_eval){
  /*Evaluate the outermost (1/6 and 5/6) of the sought interval*/
  double f1 = f(a+(b-a)/6), f4 = f(a+5*(b-a)/6);
  *int_eval = *int_eval + 2;
  /*Calcualte the approximate integral of the interval using the trapez rectangle rules*/
  /*Utilize that the function calls om the lower order method are imbedded in the higher order method*/
  double Q = (2*f1+f2+f3+2*f4)/6*(b-a), q = (f1+f2+f3+f4)/4*(b-a);
  double tol = acc+eps*fabs(Q), error=fabs(Q-q);
  /*Only save the error if the approximation is accepted*/
  if(error<tol){*err+=error; return Q;}
  /*Since the precision is not good enough split into two intervals, and integragte these recursicely
    Note that with the choice of evaluationpoints f1 and f2 now corresponds to
    f2 and f3 in the first of the new two subintervals. The same goes for f3 and f4,
    and the second of the new sub intervals*/
  else{
    *nrec = *nrec + 1;
    double Q1 = adapt_integ(f, a, (b+a)/2, acc/sqrt(2.0), eps, f1, f2, err, nrec, int_eval);
    double Q2 = adapt_integ(f, (b+a)/2, b, acc/sqrt(2.0), eps, f3, f4, err, nrec, int_eval);
    return Q1+Q2;
  }
}

double integ_helper(double f(double), double a, double b, double acc, double eps, double* err, int* nrec, int* int_eval){
  /*Initiate f2 and f3*/
  double f2 = f(a+2*(b-a)/6), f3 = f(a+4*(b-a)/6);
  *nrec = 0;
  *int_eval = 0;
  *int_eval = *int_eval + 2;
  return adapt_integ(f, a, b, acc, eps, f2, f3, err, nrec, int_eval);
}

double clenshaw_curtis(double f(double), double a, double b, double acc, double eps, double* err, int* nrec, int* int_eval){
  double f_clen(double x){
    return f(cos(x))*sin(x);
  }
  double a_clens = acos(b), b_clens = acos(a);
  double f2 = f_clen(a_clens+2*(b_clens-a_clens)/6), f3 = f_clen(a_clens+4*(b_clens-a_clens)/6);
  *nrec = 0;
  *int_eval = 0;
  *int_eval = *int_eval + 2;
  return adapt_integ(f_clen, a_clens, b_clens, acc, eps, f2, f3, err, nrec, int_eval);
}

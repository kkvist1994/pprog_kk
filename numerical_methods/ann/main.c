#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"ann.h"
#include<gsl/gsl_blas.h>

double test_func(double x){
  return sin(x) + 0.5*x - 1;
}

void plot_interp(double a, double b, double dx, ann* network, gsl_vector* xlist,
    gsl_vector* ylist)
    {
      FILE* pointstream = fopen("points.txt", "w");
      for(int i = 0; i < xlist->size; i++){
        fprintf(pointstream, "%.5f\t%.5f\t\n",
        gsl_vector_get(xlist, i), gsl_vector_get(ylist, i));
      }
      fclose(pointstream);
      FILE* datastream = fopen("data.txt", "w");
      double y_i, yd_i, yad_i, x_i = a;
      while(x_i<=b){
        y_i = ann_feed_forward(network, x_i);
        yd_i = ann_deriv(network, x_i);
        yad_i = ann_integ(network, x_i, 0, x_i);
        fprintf(datastream, "%.5f\t%.5f\t%.5f\t%.5f\n", x_i, y_i, yd_i, yad_i);
        x_i+=dx;
      }
      fclose(datastream);
    }

int main(void){
  ann* network = ann_alloc(5, gaussian_wavelet);
  double Dx = 0.5, a = 0, b = 6.2, dx = 0.01;
  int size = (int) (floor((b-a)/Dx) + 1);
  gsl_vector* x = gsl_vector_alloc(size);
  gsl_vector* y = gsl_vector_alloc(size);
  initialize_xy(a, x, y, test_func, Dx);
  ann_train(network, x, y);
  plot_interp(a, b, dx, network, x, y);
  ann_free(network);
  printf("I have made a set of x and y point whoch are sampled from the\n");
  printf("the function sin(x) + 0.5*x - 1. I have used this set to train\n");
  printf("an artificial neural network. Afterwards I have interpolated the\n");
  printf("points with the trained neural network, and claculated the\n");
  printf("The derivative and antiderevative of the function resulting\n");
  printf("from the training, has also been calculated. It is all shown in\n");
  printf("the plot in the file plot.svg.");
  gsl_vector_free(x);
  gsl_vector_free(y);
  return 0;
}

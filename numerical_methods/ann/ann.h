typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;
typedef struct {ann* network; gsl_vector* x; gsl_vector* y;} parameters;
void print_vector(const gsl_vector*);
ann* ann_alloc(int, double (*)(double));
void ann_free(ann*);
double ann_feed_forward(ann*, double);
void ann_train(ann*, gsl_vector*, gsl_vector*);
double gaussian_wavelet(double);
double gaussian(double);
double wavelet(double);
void initialize_xy(double, gsl_vector*, gsl_vector*, double (double), double);
double ann_deriv(ann*, double);
double ann_integ(ann*, double, double, double);

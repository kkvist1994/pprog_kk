#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"ann.h"
#include"integ.h"
#include<gsl/gsl_blas.h>
#include<gsl/gsl_multimin.h>

void print_vector(const gsl_vector* v){
  printf("[");
  for(int i = 0; i < v->size; i++){
    if (i != v->size-1) printf("%.3f\n", gsl_vector_get(v, i));
    else printf("%.3f]\n\n", gsl_vector_get(v, i));
  }
}

double gaussian_wavelet(double x){
  return x*exp(-x*x);
}

double gaussian(double x){
  return exp(-x*x);
}

double wavelet(double x){
  return cos(5*x)*exp(-x*x) + x;
}

ann* ann_alloc(int hn, double (*act_fun)(double)){
  ann* network = malloc(sizeof(ann));
  network->n = hn;
	network->f = act_fun;
	network->data = gsl_vector_alloc(3*hn);
	return network;
}

void ann_free(ann* network){
	gsl_vector_free(network->data);
	free(network);
}

double ann_feed_forward(ann* network, double x){
  double y = 0, a_i, b_i, w_i;
  int i, k;
  for(i = 0; i<network->n; i++){
    /*The index k will give the index of parameter a of hidden neuron i*/
    k = 3*i;
    a_i = gsl_vector_get(network->data, k);
    b_i = gsl_vector_get(network->data, k+1);
    w_i = gsl_vector_get(network->data, k+2);
    y+=(network->f)((x-a_i)/b_i)*w_i;
  }
  return y;
}

double ann_deriv(ann* network, double x){
  double h = 0.01;
  double yx = ann_feed_forward(network, x);
  double yxh = ann_feed_forward(network, x+h);
  double dfdx = (yxh - yx)/h;
  return dfdx;
}

double ann_integ(ann* network, double x, double a, double b){
  double integ = 0, a_i, b_i, w_i;
  int i, k;
  double err = 0, acc = 1e-10, eps = 1e-10;
  int nrec = 0, int_eval;
  for(i = 0; i<network->n; i++){
    /*The index k will give the index of parameter a of hidden neuron i*/
    k = 3*i;
    a_i = gsl_vector_get(network->data, k);
    b_i = gsl_vector_get(network->data, k+1);
    w_i = gsl_vector_get(network->data, k+2);
    integ+=w_i*b_i*integ_helper(network->f, (a-a_i)/b_i, (b-a_i)/b_i, acc, eps, &err, &nrec, &int_eval);
  }
  return integ;
}

double squares_sum(const gsl_vector* v, void* params){
  parameters* p = (parameters*) params;
  gsl_vector_memcpy(p->network->data, v);
  double F_x;
  double ssum = 0;
  for(int i = 0; i<p->x->size; i++){
    F_x = ann_feed_forward(p->network, gsl_vector_get(p->x, i));
    ssum += pow(gsl_vector_get(p->y, i) - F_x, 2);
  }
  return ssum;
}

/*The following training routine could be enhaced by adding a momentum
  parameter, to avoid getting stock at a local minimum*/
void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist){
  int n = (network->n)*3;
  const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;

  size_t iter = 0;
  int status;
  double size;

/* Starting point */
  x = gsl_vector_alloc (n);
  gsl_vector_set_all(x, 1.0);

/* Set initial step sizes to 1 */
  ss = gsl_vector_alloc (n);
  gsl_vector_set_all (ss, 1.0);

/* Initialize method and iterate */
  gsl_multimin_function minex_func;
  parameters par = {network, xlist, ylist};
  minex_func.n = n;
  minex_func.f = squares_sum;
  minex_func.params = &par;

  s = gsl_multimin_fminimizer_alloc (T, n);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      if (status) {break;}
      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);
    }
  while (status == GSL_CONTINUE && iter < 1000);

  gsl_vector_memcpy(network->data, s->x);
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free(s);
}

void initialize_xy(double a, gsl_vector* x, gsl_vector* y, double f(double), double Dx){
  double x_i = a;
  for(int i = 0; i<x->size; i++){
    gsl_vector_set(x, i, x_i);
    gsl_vector_set(y, i, f(x_i));
    x_i+=Dx;
  }
}

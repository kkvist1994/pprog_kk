#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<time.h>
#include"lanczos.h"

void print_vector(const gsl_vector* v){
  printf("[");
  for(int i = 0; i < v->size; i++){
    if (i != v->size-1) printf("%.3f\n", gsl_vector_get(v, i));
    else printf("%.3f]\n\n", gsl_vector_get(v, i));
  }
}

void print_matrix(const gsl_matrix* m){
  printf("[");
  for(int i = 0; i < m->size1; i++){
    for(int j = 0; j < m->size2; j++){
      if (j == m->size2-1){
        if (i == m->size1-1) printf("%.3f]\n\n", gsl_matrix_get(m, i, j));
        else printf("%.3f\n", gsl_matrix_get(m, i, j));
      }
      else printf("%.3f ", gsl_matrix_get(m, i, j));
    }
  }
}

void randomize_symmetric(gsl_matrix* m, unsigned int* seed){
  double val;
  for(int i = 0; i < m->size1; i++){
    for(int j = i; j < m->size2; j++){
      val = 10.0*((double) rand_r(seed))/((double) RAND_MAX) - 5.0;
      gsl_matrix_set(m, i, j, val);
      if(i != j) {gsl_matrix_set(m, j, i, val);}
    }
  }
}

/*The following routine is based on ideas from:
  https://www.cas.mcmaster.ca/~qiao/publications/solanczos.pdf, and
  https://en.wikipedia.org/wiki/Lanczos_algorithm.
  It tridiagonalizes a matrix symmetric real A, such that T = QTAQ
  is tridiagonal and Q is orthogonal.
  In the following comments, q_i refers to the i'th collum of Q.
  a refers to diagonal elements of T, while b refers to offdiagonal elements.*/


void lanczos_tri_dia(gsl_matrix* A, gsl_matrix* T, gsl_matrix* Q, gsl_vector* b_v){
  /*Making a two doubles and two vectors to use for calculations*/
  double a, b;
  gsl_vector* y = gsl_vector_alloc(A->size1);
  gsl_vector* z = gsl_vector_alloc(A->size1);
  /*Normalize the starting point vector*/
  gsl_blas_ddot(b_v, b_v, &a);
  gsl_vector_scale(b_v, 1/(a));
  /*Set first row of Q*/
  gsl_matrix_set_col(Q, 0, b_v);
  gsl_blas_dgemv(CblasNoTrans, 1.0, A, b_v, 0.0, y);
  /*calculate a_1 in T and store*/
  gsl_blas_ddot(y, b_v, &a);
  gsl_matrix_set(T, 0, 0, a);
  /*Prepare y for iteration*/
  gsl_blas_daxpy(-a, b_v, y);
  for(int j = 1; j < A->size1; j++){
    /*b_j = norm(y)*/
    gsl_blas_ddot(y, y, &b);
    b = sqrt(b);
    if(b == 0){printf("B = 0, at j = %d, try new start point\n", j); return;}
    /*Store b_j in T*/
    gsl_matrix_set(T, j-1, j, b);
    gsl_matrix_set(T, j, j-1, b);
    /*store q_j = y/b_j in y and in j'th row of matrix Q*/
    gsl_vector_scale(y, 1.0/b);
    gsl_matrix_set_col(Q, j, y);
    /*Store A*q_j in z*/
    gsl_blas_dgemv(CblasNoTrans, 1.0, A, y, 0.0, z);
    /*a_j = z dot y*/
    gsl_blas_ddot(z, y, &a);
    /*Store in T*/
    gsl_matrix_set(T, j, j, a);
    /*Calculate the needed y for the next iteration*/
    gsl_vector_scale(y, -a);
    gsl_blas_daxpy(1.0, z, y);
    gsl_matrix_get_col(z, Q, j-1);
    gsl_blas_daxpy(-b, z, y);
  }
  gsl_vector_free(y);
  gsl_vector_free(z);
}

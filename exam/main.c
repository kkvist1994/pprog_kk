#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_blas.h>
#include"lanczos.h"

int main(void){
  int n = 5;
  /*I provide this seed to get new random numbers every time withoit recompilation*/
  unsigned int seed = time(NULL);
  gsl_matrix* A = gsl_matrix_alloc(n, n);
  gsl_matrix* T = gsl_matrix_alloc(n, n);
  gsl_matrix* Q = gsl_matrix_alloc(n, n);
  gsl_vector* b = gsl_vector_calloc(n);
  gsl_matrix* QTQ = gsl_matrix_alloc(n, n);
  gsl_matrix* QTA = gsl_matrix_alloc(n, n);
  gsl_matrix* QTAQ = gsl_matrix_alloc(n, n);
  /*Using the algorithm five times to show that it works on some different matrices*/
  printf("Using the algorithm on five randomly generated real symmetric matrices:\n\n");
  for(int i = 0; i<5; i++){
    printf("Iteration %d\n\n", i+1);
    randomize_symmetric(A, &seed);
    gsl_vector_set(b, 0, 1.0);
    lanczos_tri_dia(A, T, Q, b);
    printf("Matrix A:\n\n");
    print_matrix(A);
    printf("Matrix Q:\n\n");
    print_matrix(Q);
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, Q, Q, 0.0, QTQ);
    printf("Matrix QTQ:\n\n");
    /*If Q is orthonormal QTQ = I*/
    print_matrix(QTQ);
    printf("As expected QTQ gives the identity matrix since Q is orthonormal\n\n");
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, Q, A, 0.0, QTA);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, QTA, Q, 0.0, QTAQ);
    /*Printing T, and showing QTAQ reproduces it*/
    printf("Matrix T:\n\n");
    print_matrix(T);
    printf("Matrix QTAT:\n\n");
    print_matrix(QTAQ);
    printf("As expected T and QTAQ are the same\n\n");
    printf("***************************************************************\n\n");
  }
  gsl_vector_free(b);
  gsl_matrix_free(A);
  gsl_matrix_free(T);
  gsl_matrix_free(Q);
  gsl_matrix_free(QTQ);
  gsl_matrix_free(QTA);
  gsl_matrix_free(QTAQ);
  return 0;
}

void print_vector(const gsl_vector*);
void print_matrix(const gsl_matrix*);
void randomize_symmetric(gsl_matrix*, unsigned int*);
void lanczos_tri_dia(gsl_matrix*, gsl_matrix*, gsl_matrix*, gsl_vector*);

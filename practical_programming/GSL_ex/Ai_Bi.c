#include<stdio.h>
#include<gsl/gsl_sf_airy.h>
#include<math.h>
#include<stdlib.h>

int main(int argc, char* argv[]){
        for(int i=1;i<argc;i++) {
                double x=atof(argv[i]);
		double A_i = gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE);
		double B_i = gsl_sf_airy_Bi(x, GSL_PREC_DOUBLE);
                printf("%lg \t %lg \t %lg \n",x, A_i, B_i);
        }
        return 0;
}

#include<stdio.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>

gsl_vector* mat_vec_mult(gsl_matrix* m, gsl_vector* v){
	if (m->size2 != v->size){fprintf(stderr, "Invalid dimension");}
	gsl_vector* res = gsl_vector_alloc(m->size1);
	double ent;
	for(int i = 0; i < m->size1; i++){
		ent = 0;
		for(int j = 0; j < m->size2; j++){
			ent += gsl_matrix_get(m, i, j)*gsl_vector_get(v, j);
		}
		gsl_vector_set(res, i, ent);
	}
	return res;
}


void print_vector(gsl_vector* v){
	int size = v->size;
	printf("[");
	for(int i = 0; i<size; i++){
		if (i != size-1){printf("%lg \n", gsl_vector_get(v, i));}
		else{printf("%lg] \n", gsl_vector_get(v, i));}
	}
}

int main(){
	gsl_vector* b = gsl_vector_alloc(3);
	gsl_vector_set(b, 0, 6.23);
	gsl_vector_set(b, 1, 5.37);
	gsl_vector_set(b, 2, 2.29);
	gsl_vector* x = gsl_vector_alloc(3);
	gsl_matrix* A = gsl_matrix_alloc(3, 3);
	gsl_matrix_set(A, 0, 0, 6.13);
	gsl_matrix_set(A, 0, 1, -2.90);
	gsl_matrix_set(A, 0, 2, 5.86);
	gsl_matrix_set(A, 1, 0, 8.08);
	gsl_matrix_set(A, 1, 1, -6.31);
	gsl_matrix_set(A, 1, 2, -3.89);
	gsl_matrix_set(A, 2, 0, -4.36);
	gsl_matrix_set(A, 2, 1, 1.00);
	gsl_matrix_set(A, 2, 2, 0.19);
	gsl_matrix* A_copy = gsl_matrix_alloc(3,3);
	gsl_matrix_memcpy(A_copy, A);
	gsl_linalg_HH_solve(A, b, x);
	printf("Solving using Householder solver yields x = \n");
	print_vector(x);
	gsl_vector* test_vec = mat_vec_mult(A_copy, x);
	printf("For comparison multipliplying A with the solution x yields: \n");
        print_vector(test_vec);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_matrix_free(A);
	gsl_vector_free(test_vec);
	gsl_matrix_free(A_copy);
return 0;
}


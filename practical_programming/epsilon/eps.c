#include<limits.h>
#include<float.h>
#include<stdio.h>
#include"equal.h"

int main(){


int i=1; while(i+1>i) {i++;}
printf("my max int using while loop = %i\n",i);


int k = 0;
for (int i = 1; i < i+1; i++) {
k = i+1;
} 
printf("my max int using for loop is = %i\n", k);

int z = 1;
do{z++;}
while(z+1 > i); 
printf("my max int using for do while is = %i\n", z);

printf("For comparison INT_MAX = %i\n", INT_MAX);


int i_min=0; while(i_min-1<i_min) {i_min--;}
printf("my min int using while loop = %i\n",i_min);


int k_min = 0;
for (int i = 0; i-1 < i; i--) {
k_min = i-1;
} 
printf("my min int using for loop is = %i\n", k_min);

int z_min = 0;
do{z_min--;}
while(z_min-1 < z_min); 
printf("my min int using for do while is = %i\n", z_min);

printf("For comparison INT_MIN = %i\n", INT_MIN);


float x_f=1; while(1+x_f!=1){x_f/=2;} x_f*=2;
printf("The double machine epsilon using while loop is: %.25f\n", x_f);

float e_f; for(e_f=1; 1+e_f!=1; e_f/=2){} e_f*=2;
printf("The double machine epsilon using for loop is: %.25f\n", e_f);

float y_f=1; do{y_f/=2;} while(1+y_f!=1); y_f*=2;
printf("The double machine epsilon using do  while loop is: %.25f\n", y_f);

printf("For comparison FLT_EPSILON = %g\n", FLT_EPSILON);

double x_d=1; while(1+x_d!=1){x_d/=2;} x_d*=2;
printf("The double machine epsilon using while loop is: %.25g\n", x_d);

double e_d; for(e_d=1; 1+e_d!=1; e_d/=2){} e_d*=2;
printf("The double machine epsilon using for loop is: %.25g\n", e_d);

double y_d=1; do{y_d/=2;} while(1+y_d!=1); y_d*=2;
printf("The double machine epsilon using do  while loop is: %.25g\n", y_d);

printf("For comparison DBL_EPSILON = %.25g\n", DBL_EPSILON);

long double x_ld=1; while(1+x_ld!=1){x_ld/=2;} x_ld*=2;
printf("The double machine epsilon using while loop is: %.25Lg\n", x_ld);

long double e_ld; for(e_ld=1; 1+e_ld!=1; e_ld/=2){} e_ld*=2;
printf("The double machine epsilon using for loop is: %.25Lg\n", e_ld);

long double y_ld=1; do{y_ld/=2;} while(1+y_ld!=1); y_ld*=2;
printf("The double machine epsilon using do  while loop is: %.25Lg\n", y_ld);

printf("For comparison LDBL_EPSILON = %.25Lg\n", LDBL_EPSILON);


int max = INT_MAX/3;
float sum_up_float = 0.0;
for(int i = 1; i<=max; i++){
	sum_up_float += 1.0/i;
}
printf("Sum_up_float = %.25g\n", sum_up_float);

float sum_down_float = 0.0;
for(int i = max; i>0; i--){
        sum_down_float += 1.0/i;
}
printf("Sum_down_float = %.25g\n", sum_down_float);
printf("They are probably different because of rounding errors. So when you sum down, the first many contributions are too small, not to be rounded down to zero anyway\n");
printf("Since the integral of 1/x is nonconvergent neither is the sum. The harmonic series test would show this as well\n");

double sum_up_double = 0.0;
for(int i = 1; i<=max; i++){
        sum_up_double += 1.0/i;
}
printf("Sum_up_double = %.25lg\n", sum_up_double);

double sum_down_double = 0.0;
for(int i = max; i>0; i--){
        sum_down_double += 1.0/i;
}
printf("Sum_down_double = %.25lg\n", sum_down_double);

printf("When using a duouble, we have more decimals and the rounding off errors become very small.\n");


int e = equal(1.0, 1.0, 1.0, 1.0);
printf("Test equal yields: %i\n", e);


return 0;


}

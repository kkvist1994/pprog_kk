#include<stdio.h>
#include"komplex.h"
#include<math.h>

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}


komplex komplex_mult(komplex a, komplex b){
	komplex result = {a.re*b.re - a.im*b.im, a.im*b.re + b.im*a.re};
	return result;
}


int equal(double a, double b, double tau, double epsilon){
int result = 0;
if (fabs(a-b) < tau || fabs(a-b)/(fabs(a) + fabs(b)) < epsilon/2){result = 1;}
return result;
}


int komplex_equal(komplex a, komplex b, double acc, double eps){
	if (equal(a.re, b.re, acc, eps) == 1 && equal(a.im, b.im, acc, eps) == 1){
		return 1;}
	else{
		return 0;}
}

komplex komplex_exp(komplex z){
	komplex real = {exp(z.re), 0};
	komplex imaginary = {cos(z.im), sin(z.im)};
	komplex result = komplex_mult(real, imaginary);
	return result;
}

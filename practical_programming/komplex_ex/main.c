#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6
#define pi 3.14159265359

int main(){
	komplex a = {1,2}, b = {3,4};

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);
	komplex e_test1 = {0,0};
	komplex e = komplex_exp(e_test1);
	komplex_print("komplex exp with input (0,0) returns: ", e);
	komplex e_test2 = {0,pi};
        komplex e_pi = komplex_exp(e_test2);
        komplex_print("komplex exp with input (0,pi) returns: ", e_pi); 
/* the following is optional */

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passed :) \n");
	else
		printf("test 'add' failed: debug me, please... \n");
return 0;
}

#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_matrix.h>
#include<stdlib.h>

double analytic_sol(double x){
	double res;
	res = exp(x)/(exp(x)+1);
	return res;
}

int func(double x, const double y[], double f[], void* params){
	(void)(x);
	f[0] = y[0]*(1.0-y[0]);
	return GSL_SUCCESS;
}

int jac (double x, const double y[], double *dfdy, double dfdx[], void* params){
	(void)(x);
	dfdy[0] = 1.0-2*y[0];
	dfdx[0] = 0.0;
	return GSL_SUCCESS;
}

int main(void){
	FILE* mystream = fopen("simple_diff_solution.txt", "w");
	gsl_odeiv2_system sys = {func, jac, 1, NULL};
	gsl_odeiv2_driver* d =
	gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 0);
	double x = 0.0, x1 = 3.0;
	double y[1] = {0.5};
	int steps = 200;
	fprintf(mystream, "%.5lg\t%.5lg\t%.5lg\n", x, y[0], analytic_sol(x));
	for (int i = 1; i <= steps; i++){
		double xi = i*x1/(steps);
		int status = gsl_odeiv2_driver_apply(d, &x, xi, y);
		if (status != GSL_SUCCESS){
			 fprintf (stderr, "error, return value=%d\n", status);
			 break;}
		fprintf(mystream, "%.5lg\t%.5lg\t%.5lg\n", x, y[0], analytic_sol(x));
        }
	gsl_odeiv2_driver_free(d);
	fclose(mystream);
	return 0;
}


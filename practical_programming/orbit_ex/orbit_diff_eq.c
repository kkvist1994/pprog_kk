#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_matrix.h>
#include<stdlib.h>

int func(double phi, const double y[], double f[], void* params){
        (void)(phi);
	double eps = *(double *)params;
        f[0] = y[1];
	f[1] = 1.0 + eps*y[0]*y[0] - y[0];
        return GSL_SUCCESS;
}

int jac (double phi, const double y[], double *dfdy, double dfdphi[], void* params){
        (void)(phi);
	double eps = *(double *)params;
	gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy, 2, 2);
	gsl_matrix* m = &dfdy_mat.matrix;
	gsl_matrix_set(m, 0, 0, 0.0);
	gsl_matrix_set(m, 0, 1, 1.0);
	gsl_matrix_set(m, 1, 0, 2*eps*y[0]-1.0);
	gsl_matrix_set(m, 1, 1, 0.0);
        dfdphi[0] = 0.0;
        dfdphi[1] = 0.0;
        return GSL_SUCCESS;
}

int main(int argc, char* argv[]){
        FILE* mystream = fopen(argv[1], "w");
	double eps = atof(argv[2]);
        gsl_odeiv2_system sys = {func, jac, 2, &eps};
        gsl_odeiv2_driver* d =
        gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 0);
        double phi = 0.0, phi1 = 5*2*M_PI;
        double y[2] = {atof(argv[3]), atof(argv[4])};
        int steps = 1000;
        fprintf(mystream, "%.5lg\t%.5lg\n", phi, y[0]);
        for (int i = 1; i <= steps; i++){
                double phii = i*phi1/(steps);
                int status = gsl_odeiv2_driver_apply(d, &phi, phii, y);
                if (status != GSL_SUCCESS){
                         fprintf (stderr, "error, return value=%d\n", status);
                         break;}
                fprintf(mystream, "%.5lg\t%.5lg\n", phi, y[0]);
        }
        gsl_odeiv2_driver_free(d);
        fclose(mystream);
        return 0;
}


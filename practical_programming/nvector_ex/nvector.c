#include<stdio.h>
#include"nvector.h"
#include<math.h>
#include<stdlib.h>
#define TINY 1e-6

int double_equal(double a, double b){
        int result = 0;
        if (fabs(a-b) < TINY || fabs(a-b)/(fabs(a) + fabs(b)) < TINY/2){result = 1;}
        return result;
}


nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);} /* v->data is identical to (*v).data */

void nvector_set(nvector* v, int i, double value){ (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

double nvector_dot_product(nvector* u, nvector* v){
	double result = 0;
	if (u->size != v->size){fprintf(stderr, "error in dot product: Vectors do not have the same size");}
	else{
		for(int i = 0; i < v->size; i++){
			result += (u->data[i])*(v->data[i]);
		}
	}
	return result;
}

void nvector_add(nvector* a, nvector* b){
	if (a->size != b->size){fprintf(stderr, "error in dot product: Vectors do not have the same size");}
	for(int i = 0; i < a->size; i++){
		a->data[i] += b->data[i];
	}
}

int nvector_equal(nvector* a, nvector* b){
	int result = 1;
	if (a->size != b->size){fprintf(stderr, "error in dot product: Vectors do not have the same size");}
        for(int i = 0; i < a->size; i++){
                if (!(double_equal(a->data[i], b->data[i]))) {result = 0;}
        }
	return result;
}

void nvector_print(char* s, nvector* v){
	printf("%c", *s);
	for(int i = 0; i < v->size; i++){
		if (i == 0){printf("[%g, ", v->data[i]);}
		else if (i == (v->size-1)){printf("%g]\n", v->data[i]);}
		else {printf("%g, ", v->data[i]);}
	}
}


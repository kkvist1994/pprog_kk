#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdlib.h>

double dudx (double x, void* params){
   double res = 2/sqrt(M_PI)*exp(-x*x);
   return res;
}


int main(int argc, char *argv[]){
   double a = atof(argv[1]);
   double b = atof(argv[2]);
   double dx = atof(argv[3]);
   double x = a;
   double result, error;
   gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
   gsl_function F;
   F.function = &dudx;
   F.params = NULL;
   while (x <= b+dx){
      gsl_integration_qags (&F, 0, x, 1e-7, 1e-7, 1000, w, &result, &error);
      printf("%.5f %.5f\n", x, result);
      x += dx;
   }
   gsl_integration_workspace_free(w);
   return 0;
}

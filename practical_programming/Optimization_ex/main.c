#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>

double rosenbrock_f(const gsl_vector* v, void* params){
   double x, y;
   x = gsl_vector_get(v, 0);
   y = gsl_vector_get(v, 1);
   double result = pow(1.0 - x, 2) + 100*pow(y-x*x,2);
   return result;
}

void rosenbrock_df(const gsl_vector* v, void* params, gsl_vector* df){
   double x, y;
   x = gsl_vector_get(v, 0);
   y = gsl_vector_get(v, 1);
   double dfx = -2*(1-x)-400*x*(y-x*x);
   double dfy = 200*(y-x*x);
   gsl_vector_set(df, 0, dfx);
   gsl_vector_set(df, 1, dfy);
}

void rosenbrock_fdf(const gsl_vector* v, void* params, double* f, gsl_vector* df){
   *f = rosenbrock_f(v, params);
   rosenbrock_df(v, params, df);
}

void minimize_rosen(){
   int n = 2; /*Number of variables*/
   /*Inititalizing function*/
   void* p = NULL;
   gsl_multimin_function_fdf rosen_min;
   rosen_min.n = n;
   rosen_min.f = &rosenbrock_f;
   rosen_min.df = &rosenbrock_df;
   rosen_min.fdf = &rosenbrock_fdf;
   rosen_min.params = p;
   /*Setup for the minimizer*/
   size_t iter = 0;
   int status;
   const gsl_multimin_fdfminimizer_type *T;
   gsl_multimin_fdfminimizer *s;
   /*Initializing the vector holding the variable values*/
   gsl_vector* x;
   x = gsl_vector_alloc(n);
   gsl_vector_set(x, 0, 0.0);
   gsl_vector_set(x, 1, 0.0);
   /*Setting the algorith to be used and allocating workspace*/
   T = gsl_multimin_fdfminimizer_vector_bfgs2;
   s = gsl_multimin_fdfminimizer_alloc (T, n);
   /*Providing function and precision*/
   gsl_multimin_fdfminimizer_set (s, &rosen_min, x, 0.1, 1e-3);
   /*Iterating until a minimum is found, or too many iterations has been done*/
   do{
      iter++;
      gsl_multimin_fdfminimizer_iterate (s);
      status = gsl_multimin_test_gradient (s->gradient, 1e-3);
      if (status == GSL_SUCCESS){printf("Minimum found at:\n");}
      printf ("Iter = %5ld x = %.5f y = %.5f f(x,y) = %10.5f\n", iter,
              gsl_vector_get (s->x, 0),
              gsl_vector_get (s->x, 1),
              s->f);
   }
   while (status == GSL_CONTINUE && iter < 1000);
   gsl_multimin_fdfminimizer_free (s);
   gsl_vector_free (x);
}

struct experimental_data {int n; double *t,*y,*e;};

struct fit_parameters {double A; double T; double B;};

double exponential(double t, double A, double T, double B){
   double result = A*exp(-(t)/T) + B;
   return result;
}

double function_to_minimize (const gsl_vector *x, void *params) {
        double  A = gsl_vector_get(x,0);
        double  T = gsl_vector_get(x,1);
        double  B = gsl_vector_get(x,2);
        struct experimental_data *p = (struct experimental_data*) params;
        int     n = p->n;
        double *t = p->t;
        double *y = p->y;
        double *e = p->e;
        double sum=0;
        for(int i=0;i<n;i++) sum += pow( (exponential(t[i], A, T, B) - y[i])/e[i] ,2);
        return sum;
}

struct fit_parameters fit_data(int length, double* t, double* y, double* e){
   int n = 3;
   double size;
   /*Initializing function*/
   struct experimental_data params = {length, t, y, e};
   gsl_multimin_function least_squares;
   least_squares.n = n;
   least_squares.f = function_to_minimize;
   least_squares.params = &params;
   /*Setup for the minimizer*/
   size_t iter = 0;
   int status;
   const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
   gsl_multimin_fminimizer* s;
   s = gsl_multimin_fminimizer_alloc (T, n);
   /*Initializing the vector holding the variable values*/
   gsl_vector* x;
   x = gsl_vector_alloc(n);
   gsl_vector_set(x, 0, 1.0);
   gsl_vector_set(x, 1, 1.0);
   gsl_vector_set(x, 2, 1.0);
   /* Set initial step sizes to 1 */
   gsl_vector* ss;
   ss = gsl_vector_alloc (3);
   gsl_vector_set_all (ss, 0.5);
   /*Providing function and precision*/
   gsl_multimin_fminimizer_set (s, &least_squares, x, ss);
   /*Iterating until a minimum is found, or too many iterations has been done*/
   do{
      iter++;
      gsl_multimin_fminimizer_iterate (s);
      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-10);
      if (status == GSL_SUCCESS){printf("Minimum found at:\n");}
      printf ("Iter = %5ld A = %.5f T = %.5f B = %.5f, sum of squares = %.5f\n", iter,
              gsl_vector_get (s->x, 0),
              gsl_vector_get (s->x, 1),
              gsl_vector_get (s->x, 2),
              s->fval);
   }
   while (status == GSL_CONTINUE && iter < 1000);
   struct fit_parameters fit_params = {gsl_vector_get(s->x,0), gsl_vector_get(s->x,1), gsl_vector_get(s->x,2)};
   gsl_multimin_fminimizer_free (s);
   gsl_vector_free (x);
   gsl_vector_free (ss);
   return fit_params;
}


void plot_fittet_data(double dt, int length, double* t, double* y, double* e, struct fit_parameters fit_params){
   double A = fit_params.A;
   double T = fit_params.T;
   double B = fit_params.B;
   FILE* datastream = fopen("data.txt", "w");
   for (int i = 0; i < length; i++){fprintf(datastream, "%.5f %.5f %.5f\n", t[i], y[i], e[i]);}
   fclose(datastream);
   FILE* fitstream = fopen("fit.txt", "w");
   for (double d = 0; d<=1.1*t[length-1]; d += dt){
      fprintf(fitstream, "%.5f %.5f\n", d, exponential(d, A, T, B));
   }
   fclose(fitstream);
}

int main(){
   printf("*****Finding minimum of the rosenbrock function*****\n");
   minimize_rosen();
   printf("*****Fitting the activity of the radioactive substance*****\n");
   double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
   double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
   double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
   int length = sizeof(t)/sizeof(t[0]);
   struct fit_parameters fit_params = fit_data(length, t, y, e);
   plot_fittet_data(0.05, length, t, y, e, fit_params);
   return 0;
}

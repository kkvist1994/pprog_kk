*****Finding minimum of the rosenbrock function*****
Iter =     1 x = 0.16126 y = 0.00000 f(x,y) =    0.77111
Iter =     2 x = 0.29283 y = 0.05059 f(x,y) =    0.62369
Iter =     3 x = 0.34460 y = 0.12707 f(x,y) =    0.43646
Iter =     4 x = 0.46107 y = 0.19620 f(x,y) =    0.31729
Iter =     5 x = 0.53848 y = 0.26516 f(x,y) =    0.27450
Iter =     6 x = 0.87165 y = 0.75508 f(x,y) =    0.01868
Iter =     7 x = 0.86824 y = 0.75250 f(x,y) =    0.01754
Iter =     8 x = 0.93919 y = 0.87710 f(x,y) =    0.00618
Iter =     9 x = 0.96962 y = 0.94178 f(x,y) =    0.00119
Iter =    10 x = 0.99042 y = 0.98022 f(x,y) =    0.00014
Iter =    11 x = 1.00080 y = 1.00139 f(x,y) =    0.00000
Iter =    12 x = 0.99993 y = 0.99985 f(x,y) =    0.00000
Minimum found at:
Iter =    13 x = 1.00000 y = 1.00000 f(x,y) =    0.00000
*****Fitting the activity of the radioactive substance*****
Iter =     1 A = 1.00000 T = 1.00000 B = 1.50000, sum of squares = 749.89472
Iter =     2 A = 0.33333 T = 1.83333 B = 1.83333, sum of squares = 647.37181
Iter =     3 A = 0.77778 T = 1.27778 B = 2.11111, sum of squares = 584.27053
Iter =     4 A = 0.77778 T = 1.27778 B = 2.11111, sum of squares = 584.27053
Iter =     5 A = 0.77778 T = 1.27778 B = 2.11111, sum of squares = 584.27053
Iter =     6 A = 0.77778 T = 1.27778 B = 2.11111, sum of squares = 584.27053
Iter =     7 A = 1.40466 T = 0.85528 B = 1.78532, sum of squares = 578.89554
Iter =     8 A = 1.04584 T = 1.31539 B = 1.71113, sum of squares = 573.18479
Iter =     9 A = 1.83322 T = 0.94227 B = 2.02732, sum of squares = 490.29938
Iter =    10 A = 1.83322 T = 0.94227 B = 2.02732, sum of squares = 490.29938
Iter =    11 A = 1.24994 T = 1.42163 B = 2.02444, sum of squares = 487.49175
Iter =    12 A = 1.24994 T = 1.42163 B = 2.02444, sum of squares = 487.49175
Iter =    13 A = 2.44755 T = 1.15160 B = 2.25779, sum of squares = 467.69487
Iter =    14 A = 2.39055 T = 1.30927 B = 1.99274, sum of squares = 352.42007
Iter =    15 A = 2.39055 T = 1.30927 B = 1.99274, sum of squares = 352.42007
Iter =    16 A = 2.39055 T = 1.30927 B = 1.99274, sum of squares = 352.42007
Iter =    17 A = 3.18002 T = 1.96846 B = 1.87973, sum of squares = 188.44814
Iter =    18 A = 3.18002 T = 1.96846 B = 1.87973, sum of squares = 188.44814
Iter =    19 A = 3.18002 T = 1.96846 B = 1.87973, sum of squares = 188.44814
Iter =    20 A = 4.97740 T = 2.57981 B = 1.43494, sum of squares = 55.91559
Iter =    21 A = 4.97740 T = 2.57981 B = 1.43494, sum of squares = 55.91559
Iter =    22 A = 4.97740 T = 2.57981 B = 1.43494, sum of squares = 55.91559
Iter =    23 A = 4.99425 T = 2.94042 B = 1.13635, sum of squares = 14.51197
Iter =    24 A = 4.99425 T = 2.94042 B = 1.13635, sum of squares = 14.51197
Iter =    25 A = 5.41708 T = 2.36468 B = 1.30932, sum of squares = 14.07607
Iter =    26 A = 5.41708 T = 2.36468 B = 1.30932, sum of squares = 14.07607
Iter =    27 A = 5.52009 T = 2.73920 B = 1.06993, sum of squares = 10.40495
Iter =    28 A = 4.97163 T = 2.60281 B = 1.18501, sum of squares = 4.69155
Iter =    29 A = 4.97163 T = 2.60281 B = 1.18501, sum of squares = 4.69155
Iter =    30 A = 4.97163 T = 2.60281 B = 1.18501, sum of squares = 4.69155
Iter =    31 A = 4.97163 T = 2.60281 B = 1.18501, sum of squares = 4.69155
Iter =    32 A = 4.97163 T = 2.60281 B = 1.18501, sum of squares = 4.69155
Iter =    33 A = 5.19879 T = 2.70919 B = 1.05429, sum of squares = 3.49247
Iter =    34 A = 5.02446 T = 2.84805 B = 1.04308, sum of squares = 3.25449
Iter =    35 A = 5.02446 T = 2.84805 B = 1.04308, sum of squares = 3.25449
Iter =    36 A = 5.02446 T = 2.84805 B = 1.04308, sum of squares = 3.25449
Iter =    37 A = 5.15352 T = 2.71905 B = 1.07222, sum of squares = 3.04993
Iter =    38 A = 5.15893 T = 2.70650 B = 1.09099, sum of squares = 3.00125
Iter =    39 A = 5.15893 T = 2.70650 B = 1.09099, sum of squares = 3.00125
Iter =    40 A = 5.15893 T = 2.70650 B = 1.09099, sum of squares = 3.00125
Iter =    41 A = 5.13097 T = 2.72656 B = 1.07830, sum of squares = 2.98316
Iter =    42 A = 5.13097 T = 2.72656 B = 1.07830, sum of squares = 2.98316
Iter =    43 A = 5.10911 T = 2.72106 B = 1.08964, sum of squares = 2.98288
Iter =    44 A = 5.13653 T = 2.71925 B = 1.08630, sum of squares = 2.97207
Iter =    45 A = 5.13653 T = 2.71925 B = 1.08630, sum of squares = 2.97207
Iter =    46 A = 5.13122 T = 2.71936 B = 1.08391, sum of squares = 2.97205
Iter =    47 A = 5.13122 T = 2.71936 B = 1.08391, sum of squares = 2.97205
Iter =    48 A = 5.12149 T = 2.72938 B = 1.08311, sum of squares = 2.97084
Iter =    49 A = 5.12149 T = 2.72938 B = 1.08311, sum of squares = 2.97084
Iter =    50 A = 5.13297 T = 2.72258 B = 1.08436, sum of squares = 2.96980
Iter =    51 A = 5.13297 T = 2.72258 B = 1.08436, sum of squares = 2.96980
Iter =    52 A = 5.13297 T = 2.72258 B = 1.08436, sum of squares = 2.96980
Iter =    53 A = 5.12666 T = 2.72682 B = 1.08314, sum of squares = 2.96971
Iter =    54 A = 5.13057 T = 2.72433 B = 1.08319, sum of squares = 2.96962
Iter =    55 A = 5.13057 T = 2.72433 B = 1.08319, sum of squares = 2.96962
Iter =    56 A = 5.13072 T = 2.72350 B = 1.08411, sum of squares = 2.96951
Iter =    57 A = 5.13072 T = 2.72350 B = 1.08411, sum of squares = 2.96951
Iter =    58 A = 5.13072 T = 2.72350 B = 1.08411, sum of squares = 2.96951
Iter =    59 A = 5.13072 T = 2.72350 B = 1.08411, sum of squares = 2.96951
Iter =    60 A = 5.13021 T = 2.72400 B = 1.08363, sum of squares = 2.96950
Iter =    61 A = 5.12965 T = 2.72423 B = 1.08377, sum of squares = 2.96948
Iter =    62 A = 5.13000 T = 2.72313 B = 1.08426, sum of squares = 2.96948
Iter =    63 A = 5.13000 T = 2.72313 B = 1.08426, sum of squares = 2.96948
Iter =    64 A = 5.13011 T = 2.72383 B = 1.08382, sum of squares = 2.96948
Iter =    65 A = 5.12990 T = 2.72388 B = 1.08390, sum of squares = 2.96947
Iter =    66 A = 5.13017 T = 2.72363 B = 1.08400, sum of squares = 2.96947
Iter =    67 A = 5.13003 T = 2.72345 B = 1.08408, sum of squares = 2.96947
Iter =    68 A = 5.13003 T = 2.72345 B = 1.08408, sum of squares = 2.96947
Iter =    69 A = 5.13003 T = 2.72345 B = 1.08408, sum of squares = 2.96947
Iter =    70 A = 5.13012 T = 2.72361 B = 1.08398, sum of squares = 2.96947
Iter =    71 A = 5.13012 T = 2.72361 B = 1.08398, sum of squares = 2.96947
Iter =    72 A = 5.13012 T = 2.72361 B = 1.08398, sum of squares = 2.96947
Iter =    73 A = 5.13010 T = 2.72355 B = 1.08400, sum of squares = 2.96947
Iter =    74 A = 5.13010 T = 2.72348 B = 1.08404, sum of squares = 2.96947
Iter =    75 A = 5.13010 T = 2.72348 B = 1.08404, sum of squares = 2.96947
Iter =    76 A = 5.13010 T = 2.72348 B = 1.08404, sum of squares = 2.96947
Iter =    77 A = 5.13006 T = 2.72352 B = 1.08404, sum of squares = 2.96947
Iter =    78 A = 5.13012 T = 2.72349 B = 1.08404, sum of squares = 2.96947
Iter =    79 A = 5.13010 T = 2.72353 B = 1.08402, sum of squares = 2.96947
Iter =    80 A = 5.13010 T = 2.72353 B = 1.08402, sum of squares = 2.96947
Iter =    81 A = 5.13008 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    82 A = 5.13011 T = 2.72351 B = 1.08403, sum of squares = 2.96947
Iter =    83 A = 5.13011 T = 2.72351 B = 1.08403, sum of squares = 2.96947
Iter =    84 A = 5.13010 T = 2.72353 B = 1.08402, sum of squares = 2.96947
Iter =    85 A = 5.13010 T = 2.72353 B = 1.08402, sum of squares = 2.96947
Iter =    86 A = 5.13010 T = 2.72353 B = 1.08403, sum of squares = 2.96947
Iter =    87 A = 5.13010 T = 2.72353 B = 1.08403, sum of squares = 2.96947
Iter =    88 A = 5.13010 T = 2.72353 B = 1.08403, sum of squares = 2.96947
Iter =    89 A = 5.13010 T = 2.72353 B = 1.08403, sum of squares = 2.96947
Iter =    90 A = 5.13010 T = 2.72353 B = 1.08403, sum of squares = 2.96947
Iter =    91 A = 5.13010 T = 2.72353 B = 1.08403, sum of squares = 2.96947
Iter =    92 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    93 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    94 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    95 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    96 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    97 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    98 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =    99 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   100 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   101 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   102 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   103 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   104 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   105 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   106 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   107 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   108 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   109 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   110 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   111 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   112 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   113 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   114 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   115 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   116 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   117 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   118 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   119 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   120 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   121 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   122 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   123 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   124 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   125 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   126 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   127 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   128 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   129 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   130 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   131 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   132 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   133 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   134 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   135 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   136 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Iter =   137 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947
Minimum found at:
Iter =   138 A = 5.13010 T = 2.72352 B = 1.08403, sum of squares = 2.96947

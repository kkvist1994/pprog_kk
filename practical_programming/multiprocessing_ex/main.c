#include<math.h>
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<pthread.h>
#include<omp.h>

struct my_params {int* hits; int n; unsigned int* seed;};

void* throw_n_darts(void* params){
   struct my_params* p = (struct my_params*) params;
   double hits_n_darts = 0;
   for (int i = 0; i < p->n; i++){
      double x = 2*((double) rand_r(p->seed))/RAND_MAX - 1;
      double y = 2*((double) rand_r(p->seed))/RAND_MAX - 1;
      if (fabs(y) <= sqrt(1-pow(x,2))){hits_n_darts++;}
   }
   *p->hits += hits_n_darts;
   return NULL;
}

void pthread_pi(){
   unsigned int initial_seed = time(NULL);
   int iter = 100000000;
   int n_threads = 4;
   int total_hits = 0;
   int hits[n_threads];
   int n = iter/n_threads;
   pthread_t threads[n_threads];
   unsigned int seeds[n_threads];
   for (int i = 0; i < n_threads; i++){
      hits[i] = 0;
      seeds[i] = initial_seed + i;
      struct my_params p = {&hits[i], n, &seeds[i]};
      pthread_create(&threads[i], NULL, throw_n_darts, &p);
   }
   for (int i = 1; i < n_threads; i++){
      total_hits += hits[i];
      pthread_join(threads[i], NULL);
   }
   double pi_esti = 4*((double) total_hits)/((double) iter);
   printf("Pi is estimated to: %.5f\n", pi_esti);

}

double OpenMP_pi(int iter){
   unsigned int seed = time(NULL);
   int hits = 0;
   #pragma omp parallel for reduction(+:hits)
   for (int i = 0; i < iter; i++){
      seed += i;
      double x = 2*((double) rand_r(&seed))/RAND_MAX - 1;
      double y = 2*((double) rand_r(&seed))/RAND_MAX - 1;
      if (fabs(y) <= sqrt(1-pow(x,2))){hits++;}
   }
   double pi_esti = 4*((double) hits)/((double) iter);
   return pi_esti;
}

void make_conv_plot(){
   FILE* mystream = fopen("plot.txt", "w");
   int i = 10;
   while (i <= 100000000){
      fprintf(mystream, "%d %.10f %.10f\n", i, OpenMP_pi(i), M_PI);
      i *= 2;
   }
   fclose(mystream);
}

int main(){
   printf("Give an estimate of pi using pthreads and 1000000 iterations.\n");
   pthread_pi();
   printf("Using OpenMP_pi to give an estimate of pi, for different numbers of iterations and plotting to show the convergence.\n");
   make_conv_plot();
   return 0;
}

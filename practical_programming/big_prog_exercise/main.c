#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_matrix.h>
#include<stdlib.h>

/*Define the equations to be solved and the jacobian*/

int func(double x, const double y[], double f[], void* params){
        (void)(x);
        f[0] = y[1];
        f[1] = -y[0];
        return GSL_SUCCESS;
}

int jac (double x, const double y[], double *dfdy, double dfdphi[], void* params){
        (void)(x);
        gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy, 2, 2);
        gsl_matrix* m = &dfdy_mat.matrix;
        gsl_matrix_set(m, 0, 0, 0.0);
        gsl_matrix_set(m, 0, 1, 1.0);
        gsl_matrix_set(m, 1, 0, -1.0);
        gsl_matrix_set(m, 1, 1, 0.0);
        dfdphi[0] = 0.0;
        dfdphi[1] = 0.0;
        return GSL_SUCCESS;
}

double cos_aux(double x1, gsl_odeiv2_driver* d){
	double x = 0.0;
	/*Set initial conditions and solve from 0 to x1*/
	double y[2] = {1.0, 0.0};
	int status = gsl_odeiv2_driver_apply(d, &x, x1, y);
	if (status != GSL_SUCCESS){fprintf (stderr, "error, status=%d\n", status);}
        return y[0];
}

double new_cos(double x, gsl_odeiv2_driver* d){
	/*Exploit that cosine is even*/
	double x_mod = fmod(fabs(x), 2*M_PI);
	double res;
	/*Rewrite the cosine so that cos_aux is only called with x between 0 and pi/2*/
	if (0 <= x_mod && x_mod < 0.5*M_PI){res = cos_aux(x_mod, d);}
	else if (0.5*M_PI <= x_mod && x_mod < M_PI) {res = -cos_aux(M_PI-x_mod, d);}
	else if (M_PI <= x_mod && x_mod < 1.5*M_PI) {res = -cos_aux(x_mod-M_PI, d);}
	else if (1.5*M_PI <= x_mod && x_mod < 2*M_PI) {res = cos_aux(2*M_PI-x_mod, d);}
	else{fprintf(stderr, "Mod failed");}
	return res;
}

int main(void){
	void* p = NULL;
	/*Initialize ODE solver*/
	gsl_odeiv2_system sys = {func, jac, 2, p};
        gsl_odeiv2_driver* d =
        gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 0);
	/*Set plot boundaries*/
	double a = -2*M_PI - 0.5, b = 2*M_PI + 0.5;
	double dx = 0.01;
	double x = a;
	/*Calculate data for plot and save in data.txt*/
	FILE* mystream  = fopen("data.txt", "w");
	while(x <= b-dx){
		double cos_x = new_cos(x, d);
		fprintf(mystream, "%.5f %.5f %.5f\n", x, cos_x, cos(x));
		x += dx;
	}
	gsl_odeiv2_driver_free(d);
        fclose(mystream);
	return 0;
}

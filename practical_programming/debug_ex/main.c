#include"stdio.h"
#include"gsl/gsl_matrix.h"
#include"string.h"

int print_half_00(gsl_matrix* m)
{
	double half = 0.5;
	int status = printf( "half m_{00} = %.0f\n", gsl_matrix_get(m,0,0)*half );
	return status;
}

int main(void)
{
	gsl_matrix* m = gsl_matrix_alloc(1,1);
	gsl_matrix_set(m,0,0,66);
	printf("half m_{00} (should be 33):\n");
	int status = print_half_00(m);
	size_t status_exp = strlen("half m_{00} = 33\n");
	if(status != status_exp)
		printf("status=%d : SOMETHING WENT TERRIBLY WRONG (status != status_exp)\n", status);
	else
		printf("status=%d : everything went just fine (status == status_exp)\n", status);
	gsl_matrix_free(m);
return 0;
}

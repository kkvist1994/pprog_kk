#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdlib.h>
#include"integ.h"

double f_question_a (double x, void* params){
   double res = log(x)/sqrt(x);
   return res;
}

double f_expectation (double x, void* params){
   double alpha = *(double *) params;
   double res = (-alpha*alpha*x*x/2 + alpha/2 + x*x/2)*exp(-alpha*x*x);
   return res;
}

double f_norm (double x, void* params){
   double alpha = *(double *) params;
   double res = exp(-alpha*x*x);
   return res;
}

double alpha_energy(double alpha, gsl_integration_workspace* w){
   double exp_result, exp_error, norm_result, norm_error;
   gsl_function F_norm, F_exp;
   F_exp.function = &f_expectation;
   F_exp.params = &alpha;
   F_norm.function = &f_norm;
   F_norm.params = &alpha;
   gsl_integration_qagi(&F_exp, 1e-7, 1e-7, 1000, w, &exp_result, &exp_error);
   gsl_integration_qagi(&F_norm, 1e-7, 1e-7, 1000, w, &norm_result, &norm_error);
   double res = exp_result/norm_result;
   return res;
}

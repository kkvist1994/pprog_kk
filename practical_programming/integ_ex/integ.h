#include<gsl/gsl_integration.h>

double f_question_a (double, void*);
double f_expectation (double, void*);
double f_norm (double, void*);
double alpha_energy(double, gsl_integration_workspace*);

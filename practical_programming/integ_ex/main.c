#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdlib.h>
#include"integ.h"

int main(void) {
   gsl_integration_workspace * w = gsl_integration_workspace_alloc(1000);
   /* Part a */
   double result, error;
   double *params = NULL;
   gsl_function F_a;
   F_a.function = &f_question_a;
   F_a.params = params;
   gsl_integration_qags(&F_a, 0, 1, 1e-7, 1e-7, 1000, w, &result, &error);
   printf ("result          = % .18f\n", result);
   printf ("estimated error = % .18f\n", error);
   printf ("intervals       = %zu\n", w->size);
   /* Part b */
   FILE* mystream = fopen("energy_exp.txt", "w");
   const double alpha1 = 0.1;
   const double alpha2 = 3;
   const double dalpha = 0.05;
   double alpha = alpha1;
   double res;
   while (alpha <= alpha2){
      res = alpha_energy(alpha, w);
      fprintf(mystream, "%.10lg\t%.10lg\n", alpha, res);
      alpha += dalpha;
   }
   gsl_integration_workspace_free (w);
   fclose(mystream);
   return 0;
}

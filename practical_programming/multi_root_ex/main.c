#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include"multi.h"

int main(void){
   double rosen_params[2] = {2, 100};
   double rosen_x_init[2] = {0, 0};
   find_rosen_roots(rosen_x_init, rosen_params);
   double r_min = 1e-6, r_max = 8;
   double s_wave_r_range[2] = {r_min, r_max};
   double y_s_wave_initial[2] = {r_min-r_min*r_min, 1-2.0*r_min};
   double search_range[2] = {-0.7, -0.3};
   s_wave_roots(y_s_wave_initial, s_wave_r_range, 200.0, search_range); 
   return 0;
}







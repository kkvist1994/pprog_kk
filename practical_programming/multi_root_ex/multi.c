#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_roots.h>
#include<gsl/gsl_multiroots.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include"multi.h"


int rosenbrock_df(const gsl_vector* x, void* params, gsl_vector* f){
   double p1 = ((double*)params)[0];
   double p2 = ((double*)params)[1];
   const double x0 = gsl_vector_get (x, 0);
   const double x1 = gsl_vector_get (x, 1);
   const double y0 = -p1*(1-x0) + p2*(-2*x0)*2*(x1-x0*x0);
   const double y1 = p1*p2*(x1-x0*x0);
   gsl_vector_set(f, 0, y0);
   gsl_vector_set(f, 1, y1);
   return GSL_SUCCESS;
}

void print_state (size_t iter, gsl_multiroot_fsolver * s){
  printf ("iter = %3lu x = % .3f y = %.3f "
          "dfdx = % .3e dfdy =  % .3e\n",
          iter,
          gsl_vector_get (s->x, 0),
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->f, 0),
          gsl_vector_get (s->f, 1));
}


void find_rosen_roots(double* x_init, void* params){
   const gsl_multiroot_fsolver_type *T;
   gsl_multiroot_fsolver *s;
   int status;
   const size_t n = 2;
   size_t iter = 0;
   gsl_multiroot_function f = {&rosenbrock_df, n, params};
   gsl_vector *x = gsl_vector_alloc (n);
   gsl_vector_set (x, 0, x_init[0]);
   gsl_vector_set (x, 1, x_init[1]);
   T = gsl_multiroot_fsolver_hybrids;
   s = gsl_multiroot_fsolver_alloc (T, 2);
   gsl_multiroot_fsolver_set (s, &f, x);
   do {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);
      print_state (iter, s);
      if (status) {break;}
      status = gsl_multiroot_test_residual (s->f, 1e-7);
   }
   while (status == GSL_CONTINUE && iter < 1000);

   printf ("status = %s\n", gsl_strerror(status));

   gsl_multiroot_fsolver_free (s);
   gsl_vector_free (x);
}

int s_wave(double r, const double y[], double f[], void* params){
   (void)(r);
   double eps = *(double *) params;
   f[0] = y[1];
   f[1] = -2*y[0]*(1/r + eps);
   return GSL_SUCCESS;
}

int s_wave_jac(double r, const double y[], double* dfdy, double dfdr[], void *params){
   double eps = *(double *) params;
   gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 2, 2);
   gsl_matrix* m = &dfdy_mat.matrix;
   gsl_matrix_set(m, 0, 0, 0.0);
   gsl_matrix_set(m, 0, 1, 1.0);
   gsl_matrix_set(m, 1, 0, -2.0*(1/r + eps));
   gsl_matrix_set(m, 1, 1, 0.0);
   dfdr[0] = 0.0;
   dfdr[1] = 2*y[0]/(r*r);
   return GSL_SUCCESS;
}

double theo(double r){
   double result = r*exp(-r);
   return result;
}

double solve_s_wave(double* y, double* range, double res, double eps, int print){
   gsl_odeiv2_system sys = {s_wave, s_wave_jac, 2, &eps};
   gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 1e-6);
   double r = range[0], r1 = range[1];
   if (!print){
      int status = gsl_odeiv2_driver_apply (d, &r, r1, y);
      if (status != GSL_SUCCESS) {printf("error, return value=%d\n", status);}
   }
   else{
   FILE* mystream = fopen("root_solution.txt", "w");
   for (int i = 1; i <= res; i++){
      double ri = i*r1/res;
      int status = gsl_odeiv2_driver_apply (d, &r, ri, y);
      if (status != GSL_SUCCESS) {printf("error, return value=%d\n", status); break;}
         double expected = theo(r);
         fprintf (mystream, "%.5e %.5e %.5e %.5e\n", r, y[0], y[1], expected);
      }
   fclose(mystream);
   }
   gsl_odeiv2_driver_free (d);
   /*printf("The value at r_max is: %.5lg, with epsilon: %.5lg\n", y[0], eps);*/
   double result = y[0];
   return result;
}


double r_max_value(double eps, void* params){
   struct s_wave_params *s_params = (struct s_wave_params*) params;
   double* y_initial = s_params->y_initial;
   double y[2] = {y_initial[0], y_initial[1]};
   double* range = s_params->range;
   double res = s_params->res;
   double result = solve_s_wave(y, range, res, eps, 0);
   /*printf("In r_max_value result becomes: %.10lg\n", result);*/
   return result;
}


int s_wave_roots(double* y_initial, double* range, double res, double* search_range){
   int status;
   int iter = 0, max_iter = 100;
   double r = 0;
   const gsl_root_fsolver_type *T;
   gsl_root_fsolver *s;
   struct s_wave_params s_p = {y_initial, range, res};
   gsl_function F;
   F.function = &r_max_value;
   F.params = &s_p;
   T = gsl_root_fsolver_brent;
   s = gsl_root_fsolver_alloc (T);
   double eps_lo = search_range[0], eps_hi = search_range[1];
   gsl_root_fsolver_set (s, &F, eps_lo, eps_hi);
   do{
      iter++;
      status = gsl_root_fsolver_iterate (s);
      r = gsl_root_fsolver_root (s);
      eps_lo = gsl_root_fsolver_x_lower (s);
      eps_hi = gsl_root_fsolver_x_upper (s);
      status = gsl_root_test_interval (eps_lo, eps_hi, 1e-6, 1e-6);
      if (status == GSL_SUCCESS){printf("Converged:\n");
      printf("iterations = %5d root =  %.7f\n",iter, r);
      }}
   while (status == GSL_CONTINUE && iter < max_iter);
   gsl_root_fsolver_free (s);
   double y_for_plot[2] = {y_initial[0], y_initial[1]};
   solve_s_wave(y_for_plot, range, res, r, 1);
   return status;
}

#include<gsl/gsl_vector.h>

struct s_wave_params {double* y_initial; double* range; double res;};
int rosenbrock_df(const gsl_vector*, void*, gsl_vector*);
void print_state(size_t, gsl_multiroot_fsolver*);
void find_rosen_roots(double*, void*);
double solve_s_wave(double*, double*, double, double, int);
int s_wave_roots(double* , double*, double, double*);
double r_max_value(double, void*);

#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<complex.h>


double complex multiply_complex(double complex a, double complex b){
	double complex result = (creal(a)*creal(b) - cimag(a)*cimag(b)) + I*(creal(a)*cimag(b) + creal(b)*cimag(a));
	return result;
}


double complex exp_complex(double complex z){
        double complex exp_re = exp(creal(z)) + I*0;
        double complex exp_im = cos(cimag(z)) + I*sin(cimag(z));
        double complex result = multiply_complex(exp_re, exp_im);
        return result;
}

double complex square_root(double a){
	double complex result;
	if (a > 0) {result = sqrt(a) + I*0;}
	else {result = I*sqrt(fabs(a));} 
	return result;
}

int main() {
printf("The gamma function with 5 as argument returns: %g\n", tgamma(5));
printf("The bessel function with 0.5 as argument returns: %g\n", j1(0.5)); 
double complex root = square_root(-2);
printf("The square root of -2 is: I*%g\n", cimag(root));
double complex test1 = I*1;
double complex test2 = I*M_PI;
double complex exp_i = exp_complex(test1);
double complex exp_ipi = exp_complex(test2);
double complex i_power_e = exp_complex(I*M_E*M_PI/2);
printf("exp(i) is %g + I*%g\n", creal(exp_i), cimag(exp_i));
printf("exp(i*pi) is %g + I*%g\n", creal(exp_ipi), cimag(exp_ipi));
printf("i^e is %g + I*%g\n", creal(i_power_e), cimag(i_power_e));
double d = 0.1111111111111111111111111111;
float f = 0.1111111111111111111111111111;
long double ld = 0.1111111111111111111111111111L;
printf("Storing in double yields: %.25lg\n", d);
printf("Storing in float yields: %.25g\n", f);
printf("Storing in long double yields: %.25Lg\n", ld); 
return 0;
}
